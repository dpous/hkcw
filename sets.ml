(* several implementations for finite sets of integers *)

open Util

module type PRE = sig
  type t
  val name: string
  val empty: t
  val union: t -> t -> t
  val inter: t -> t -> t
  val singleton: int -> t
  val mem: int -> t -> bool
  val equal: t -> t -> bool
  val equal': t -> t -> int option
  val compare: t -> t -> int
  val full: int -> t
  val hash: t -> int
  val fold: (int -> 'a -> 'a) -> t -> 'a -> 'a
  val shift: int -> t -> t
  val size: t -> int
  val rem: int -> t -> t
  val elements: t -> int list
end

module type T = sig
  include PRE
  val add: int -> t -> t
  val is_empty: t -> bool
  val intersect: t -> t -> bool
  val diff: t -> t -> t
  val subseteq: t -> t -> bool
  val set_compare: t -> t -> [`Lt|`Eq|`Gt|`N]
  val map: (int -> int) -> t -> t
  val iter: (int -> unit) -> t -> unit
  val filter: (int -> bool) -> t -> t
  val forall: t -> (int -> bool) -> bool
  val exists: t -> (int -> bool) -> bool
  val to_list: t -> int list
  val of_list: int list -> t
  val print: out_channel -> t -> unit
  val print': (out_channel->int->unit) -> out_channel -> t -> unit
  val random: int -> float -> t
  val random': int -> t
  module Map: Hashtbl.S with type key = t
  val emptymap: string -> int -> 'a Map.t
end

module Extend(M: PRE): T = struct
  include M
  let is_empty = equal empty
  let add x = union (singleton x)
  let subseteq x y = equal (union x y) y
  let intersect x y = not (is_empty (inter x y))
  let diff x y = fold rem y x
  let set_compare x y =
    if equal x y then `Eq else
      let xy=union x y in
      if equal xy y then `Lt
      else if equal xy x then `Gt
      else `N
  let of_list = List.fold_left (fun x i -> add i x) empty 
  let to_list = elements 
  let filter f x = fold (fun i y -> if f i then add i y else y) x empty
  let iter f x = fold (fun i () -> f i) x ()
  let map f x = fold (fun i -> add (f i)) x empty
  let print g x = Printf.fprintf g "{%a}" (print_list output_int) (to_list x)
  let print' f g x = Printf.fprintf g "{%a}" (print_list f) (to_list x)
  let random n p =
    let rec aux acc = function
      | 0 -> acc
      | n ->
	if Random.float 1. < p then 
	  aux (add (n-1) acc) (n-1)
	else aux acc (n-1)
    in aux empty n
  let random' n =
    let rec loop acc = function
      | 0 -> acc
      | k -> let i = Random.int n in
             if mem i acc then loop acc k
             else loop (add i acc) (k-1)
    in
    let k = Random.int n in
    if k<=n-k then loop empty k
    else diff (full n) (loop empty (n-k))
  let forall x f =
    try fold (fun i _ -> f i || raise Not_found) x true
    with Not_found -> false
  let exists x f =
    try fold (fun i _ -> f i && raise Not_found) x false
    with Not_found -> true
  module Map = Hashtbl.Make(M)
  let emptymap name size =
    let m = Map.create size in
    (* at_exit (fun () ->
     *     Printf.printf "MAP %s:" name;
     *     let s = (Map.stats m).Hashtbl.bucket_histogram in
     *     Array.iter (Printf.printf " %i") s;
     *     Printf.printf "\n%!"
     *   ); *)
    m
end

module OList = Extend(struct 
  type t = int list
  let name = "olist"
  let empty = []
  let elements x = x 
  let rec union h k = match h,k with
    | [],l | l,[] -> l
    | x::h', y::k' -> 
      match compare x y with
	| 1 -> y::union h  k'
	| 0 -> x::union h' k'
	| _ -> x::union h' k
  let rec inter h k = match h,k with
    | [],_ | _,[] -> []
    | x::h', y::k' -> 
      match compare x y with
	| 1 ->    inter h  k'
	| 0 -> x::inter h' k'
	| _ ->    inter h' k
  let singleton x = [x]
  let equal = (=)
  let rec equal' h k = match h,k with
    | [],[] -> None
    | x::_,[] | [],x::_ -> Some x
    | x::h,y::k ->
       match compare x y with
       | 1 -> Some y
       | 0 -> equal' h k
       | _ -> Some x
  let compare = compare
  let rec mem x = function
    | [] -> false
    | y::q -> match compare x y with
	| 1 -> mem x q
	| 0 -> true 
	| _ -> false
  let fold = List.fold_right
  let shift n = List.map ((+) n)
  let hash = Hashtbl.hash
  let rec rem x = function
    | [] -> []
    | y::h as l ->
       match compare x y with
       | 1 -> y::rem x h
       | 0 -> h
       | _ -> l
  let rec full acc = function
    | 0 -> acc
    | n -> full (n-1::acc) (n-1)
  let full = full []
  let size = List.length
end)

let rec log n z =
  assert(z<>0);
  if z land 1 = 1 then n
  else log (n+1) (z lsr 1)

module Small = Extend(struct 
  type t = int
  let name = "small"
  let ws = Sys.word_size-1
  let empty = 0
  let union = (lor)
  let inter = (land)
  let singleton i = assert(i<ws); 1 lsl i
  let equal = (=)
  let equal' x y = if x=y then None else Some (log 0 (x lxor y))
  let compare = compare
  let mem i x = inter (singleton i) x <> empty
  let fold f x acc =
    let r = ref acc in
    for i = ws - 1 downto 0 do
      if mem i x then 
	r := f i !r
    done;
    !r
  let elements x = fold (fun i q -> i::q) x []
  let rem x h = h land (lnot (singleton x))
  let hash = Hashtbl.hash
  let full n = singleton n - 1
  let size x = fold (+) x 0
  let shift n m = m lsl n               (* unsafe... *)
end)

module IntList = struct 
  type t = int list
  let name = "intlist"
(*
0  -> [1]
1  -> [2]
2  -> [4]
...
n  -> [1 lsl n]
...
62 -> [2^62]
63 -> [0; 1]
...
*)
  let empty = []
  let (<::) x q = if x=0 && q=[] then [] else x::q
  let rec union x y = match x,y with 
    | [],z | z,[] -> z
    | a::x, b::y -> a lor b :: union x y
  let rec inter x y = match x,y with 
    | [],_ | _,[] -> []
    | a::x, b::y -> a land b <:: inter x y
  let rec pad acc = function 0 -> acc | n -> pad (0::acc) (n-1)
  let rec get n x = match n,x with 0,a::_ -> Some a | _,[] -> None | n,_::x -> get (n-1) x
  let width = Sys.word_size - 1 
  let singleton i = pad [1 lsl (i mod width)] (i/width)  
  let equal = (=)
  (* TODO: reprendre *)
  let rec xequal' n h k = match h,k with
    | [],[] -> None
    | x::q,[] | [],x::q ->
       if x=0 then xequal' (n+width) q []
       else Some (n+log 0 x)
    | x::h,y::k ->
       if x=y then xequal' (n+width) h k
       else Some (n+log 0 (x lxor y))
  let equal' = xequal' 0
  let compare = compare
  let mem i x = match get (i/width) x with
    | None -> false
    | Some a -> (a lsr (i mod width)) land 1 <> 0 
  let shift n x = 
    if x=[] then [] else
    let m = n mod width in
    if m=0 then pad x (n/width) else
    let wm = width-m in 
    let rec xshift b = function
      | [] -> if b=0 then [] else [b]
      | a::x -> b lor (a lsl m) :: xshift (a lsr wm) x
    in
    pad (xshift 0 x) (n/width)
  (* let shift n x =  *)
  (*   pad x (1+n/width) *)
  let fold f x acc =
    let rec aux i acc = function
      | [] -> acc
      | a::x -> aux' i width acc x a
    and aux' i w acc x = function
      | 0 -> aux (i+w) acc x
      | a -> aux' (i+1) (w-1) (if a land 1 <> 0 then f i acc else acc) x (a lsr 1)
    in aux 0 acc x
  let iter f x = fold (fun i () -> f i) x ()
  (* TOMES: *)
  (* let hash x = fold (lxor) x 0 *)
  let hash = Hashtbl.hash
  let full n = 
    let rec xfull acc = function 0 -> acc | n -> xfull (-1::acc) (n-1) in
    let m = n mod width in
    if m=0 then xfull [] (n/width) else
    xfull [1 lsl m - 1] (n/width)
  let rec diff x y = match x,y with 
    | [],_ -> []
    | _,[] -> x
    | a::x, b::y -> a land (lnot b) <:: diff x y
  let size x = fold (fun _ i -> i+1) x 0

  let is_empty = function [] -> true | _ -> false
  let rec add i = function
    | [] -> singleton i
    | x::h -> if i<width then ((1 lsl i) lor x)::h
              else x::add (i-width) h
  (* TOMES: *)
  (* let add x = union (singleton x) *)
  let rec subseteq x y = match x,y with
    | [],_ -> true
    | _,[] -> false
    | a::x, b::y -> (a lor b = b) && subseteq x y
  let rec intersect x y = match x,y with
    | [],_ | _,[] -> false
    | a::x, b::y -> (a land b != 0) || intersect x y
  let rec set_compare x y = match x,y with
    | [],[] -> `Eq
    | [],_ -> `Lt
    | _,[] -> `Gt
    | a::x, b::y -> 
      if a=b then set_compare x y else 
        let c = a lor b in 
        if (c = b) then if subseteq x y then `Lt else `N
        else if (c = a) && subseteq y x then `Gt else `N
  let of_list = List.fold_left (fun x i -> add i x) empty 
  let to_list x = List.rev (fold (fun i q -> i::q) x [])
  let elements = to_list
  let map f x = fold (fun i -> add (f i)) x empty
  let filter1 n f a =
    let a = ref a in
    let n = ref n in
    let ix = ref 1 in
    for _ = 0 to width-1 do
      if !a land !ix <> 0 && not (f !n) then
        a := !a land (lnot !ix);
      incr n;
      ix := !ix lsl 1;
    done;
    !a
  let filter =
    let rec xfilter n f = function
      | [] -> []
      | x::q -> filter1 n f x <:: xfilter (n+width) f q 
    in
    xfilter 0
  (* TOMES: *)
  (* let filter = fold (fun i x -> if f i then add i x else x) x empty *)
  let rec rem i = function
    | [] -> []
    | x::q ->
       if i<width then (x land (lnot (1 lsl i)))<::q
       else x<::rem (i-width) q
  (* TOMES: *)
  (* let rem i = filter (fun j -> i<>j) *)
  let print g x = Printf.fprintf g "{%a}" (print_list output_int) (to_list x)
  let print' f g x = Printf.fprintf g "{%a}" (print_list f) (to_list x)
  (* TOOPT *)
  let random n p =
    let rec aux acc = function
      | 0 -> acc
      | n ->
	if Random.float 1. < p then 
	  aux (add (n-1) acc) (n-1)
	else aux acc (n-1)
    in aux empty n
  let random' n =
    let rec loop acc = function
      | 0 -> acc
      | k -> let i = Random.int n in
             if mem i acc then loop acc k
             else loop (add i acc) (k-1)
    in
    let k = Random.int n in
    if k<=n-k then loop empty k
    else diff (full n) (loop empty (n-k))
  let forall x f =
    try fold (fun i _ -> f i || raise Not_found) x true
    with Not_found -> false
  let exists x f =
    try fold (fun i _ -> f i && raise Not_found) x false
    with Not_found -> true
  (* Below: using JC Filiatre maps instead of just hashtables 
     (did degrade performances on some tests) *)
  (* module Map = struct
   *   module M = Trie.Make(Ptmap)
   *   type key = t
   *   type 'a t = 'a M.t ref
   *   let create _ = ref M.empty
   *   let clear m = m := M.empty
   *   let reset = clear
   *   let copy m = ref !m
   *   let add m x y = m := M.add x y !m
   *   let remove m x = m := M.remove x !m
   *   let find m x = M.find x !m
   *   let replace = add
   *   let mem m x = M.mem x !m
   *   let iter f m = M.iter f !m
   *   let fold f m x = M.fold f !m x
   *   let stats m = failwith "unimplemented"
   *   let length m = failwith "unimplemented"
   *   let find_opt m = failwith "unimplemented"
   *   let find_all m = failwith "unimplemented"
   *   let filter_map_inplace m = failwith "unimplemented"
   * end *)
  module Map = Hashtbl.Make(struct 
    type t = int list let equal = (=) let compare = compare let hash = hash 
  end)
  let emptymap name size =
    let m = Map.create size in
    (* at_exit (fun () ->
     *     Printf.printf "MAP %s:" name;
     *     let s = (Map.stats m).Hashtbl.bucket_histogram in
     *     Array.iter (Printf.printf " %i") s;
     *     Printf.printf "\n%!"
     *   ); *)
    m
end


module IntSparseList = struct 
  type t = E | C of int * int * t
  type t_ = t
  let name = "intsparselist"
  let empty = E
  let is_empty = function E -> true | _ -> false
  let width = Sys.word_size - 1 
  let rec get n = function
    | E -> 0
    | C(m,a,q) ->
       match compare n m with
       | 1 -> get (n-m-1) q
       | 0 -> a
       | _ -> 0
  let mem i x = ((get (i/width) x) lsr (i mod width)) land 1 <> 0
  let singleton i = C(i/width,1 lsl (i mod width),E)
  let rec union x y = match x,y with 
    | E,z | z,E -> z
    | C(s,a,x), C(t,b,y) ->
       match compare s t with
       | 1 -> C(t,b,union (C(s-t-1,a,x)) y)
       | 0 -> C(s,a lor b,union x y)
       | _ -> C(s,a,union x (C(t-s-1,b,y)))
  let add i x = union (singleton i) x
  let of_list l = Util.fold add l empty
  let c s x q = match x,q with
    | 0,E -> E
    | 0,C(t,y,q) -> C(s+t+1,y,q)
    | _,_ -> C(s,x,q)
  let rec inter x y = match x,y with 
    | E,_ | _,E -> E
    | C(s,a,x'), C(t,b,y') ->
       match compare s t with
       | 1 -> inter x (c t 0 y')
       | 0 -> c s (a land b) (inter x' y')
       | _ -> inter (c s 0 x') y
  let rec diff x y = match x,y with 
    | E,_ -> E
    | _,E -> x
    | C(s,a,x'), C(t,b,y') ->
       match compare s t with
       | 1 -> diff x (c t 0 y')
       | 0 -> c s (a land (lnot b)) (diff x' y')
       | _ -> C(s,a,diff x' (C(t-s-1,b,y')))
  (* let rec pad acc = function 0 -> acc | n -> pad (0::acc) (n-1) *)
  let compare = compare
  let equal = (=)
  let rec xequal' n h k = match h,k with
    | E,E -> None
    | C(s,a,_),E | E,C(s,a,_) -> Some (width*(n+s)+log 0 a)
    | C(s,a,h),C(t,b,k) ->
       match compare s t with
       | 1 -> Some (width*(n+t)+log 0 b)
       | 0 -> 
          if a=b then xequal' (n+s+1) h k
          else Some (width*(n+s)+log 0 (a lxor b))
       | _ -> Some (width*(n+s)+log 0 a)
  let equal' = xequal' 0
  let rec fold f x acc = 
    let rec aux i acc = function
      | E -> acc
      | C(s,a,x) -> aux' (i+s*width) 0 acc x a
    and aux' i n acc x = function
      | 0 -> aux (i+width) acc x
      | a -> aux' i (n+1) (if a land 1 <> 0 then f (i+n) acc else acc) x (a lsr 1)
    in aux 0 acc x
  let iter f x = fold (fun i () -> f i) x ()
  let to_list x = List.rev (fold (fun i q -> i::q) x [])
  let elements = to_list
  (* TOOPT *)
  let shift n x = fold (fun i acc -> add (n+i) acc) x empty
  let map f x = fold (fun i -> add (f i)) x empty
  let hash = Hashtbl.hash
  let full n = 
    let rec xfull acc = function 0 -> acc | n -> xfull (C(0,-1,acc)) (n-1) in
    let m = n mod width in
    if m=0 then xfull E (n/width) else
    xfull (C(0,1 lsl m - 1,E)) (n/width)
  let size x = fold (fun _ i -> i+1) x 0

  let rec subseteq x y = match x,y with
    | E,_ -> true
    | _,E -> false
    | C(s,a,x'), C(t,b,y') ->
       match compare s t with
       | 1 -> subseteq x (c t 0 y')
       | 0 -> (a lor b = b) && subseteq x' y'
       | _ -> false
  let rec intersect x y = match x,y with
    | E,_ | _,E -> false
    | C(s,a,x'), C(t,b,y') ->
       match compare s t with
       | 1 -> intersect x (c t 0 y')
       | 0 -> (a land b != 0) || intersect x' y'
       | _ -> intersect (c s 0 x') y
  let rec set_compare x y = match x,y with
    | E,E -> `Eq
    | E,_ -> `Lt
    | _,E -> `Gt
    | C(s,a,x'), C(t,b,y') -> 
       match compare s t with
       | 1 -> if subseteq x (c t 0 y') then `Lt else `N
       | 0 -> 
          if a=b then set_compare x' y' else 
            let c = a lor b in 
            if (c = b) then if subseteq x' y' then `Lt else `N
            else if (c = a) && subseteq y' x' then `Gt else `N
       | _ -> if subseteq y (c s 0 x') then `Gt else `N
  let filter1 n f a =
    let a = ref a in
    let n = ref n in
    let ix = ref 1 in
    for _ = 0 to width-1 do
      if !a land !ix <> 0 && not (f !n) then
        a := !a land (lnot !ix);
      incr n;
      ix := !ix lsl 1;
    done;
    !a
  let filter =
    let rec xfilter n f = function
      | E -> E
      | C(s,a,q) -> c s (filter1 (n+s*width) f a) (xfilter (n+width) f q) 
    in
    xfilter 0
  (* TOMES: *)
  (* let filter = fold (fun i x -> if f i then add i x else x) x empty *)
  let rem i x = 
    let rec rem t i = function
      | E -> E
      | C(s,a,q) as x ->
         match compare s t with
         | 1 -> x
         | 0 -> c s (a land i) q
         | _ -> c s a (rem (t-s-1) i q)
    in rem (i/width) ((lnot (1 lsl (i mod width)))) x
  (* TOMES: *)
  (* let rem i = filter (fun j -> i<>j) *)
  let print g x = Printf.fprintf g "{%a}" (print_list output_int) (to_list x)
  let print' f g x = Printf.fprintf g "{%a}" (print_list f) (to_list x)
  (* TOOPT *)
  let random n p =
    let rec aux acc = function
      | 0 -> acc
      | n ->
	if Random.float 1. < p then 
	  aux (add (n-1) acc) (n-1)
	else aux acc (n-1)
    in aux empty n
  let random' n =
    let rec loop acc = function
      | 0 -> acc
      | k -> let i = Random.int n in
             if mem i acc then loop acc k
             else loop (add i acc) (k-1)
    in
    let k = Random.int n in
    if k<=n-k then loop empty k
    else diff (full n) (loop empty (n-k))
  let forall x f =
    try fold (fun i _ -> f i || raise Not_found) x true
    with Not_found -> false
  let exists x f =
    try fold (fun i _ -> f i && raise Not_found) x false
    with Not_found -> true
  (* Below: using JC Filiatre maps instead of just hashtables 
     (did degrade performances on some tests) *)
  (* module Map = struct
   *   module M = Trie.Make(Ptmap)
   *   type key = t
   *   type 'a t = 'a M.t ref
   *   let create _ = ref M.empty
   *   let clear m = m := M.empty
   *   let reset = clear
   *   let copy m = ref !m
   *   let add m x y = m := M.add x y !m
   *   let remove m x = m := M.remove x !m
   *   let find m x = M.find x !m
   *   let replace = add
   *   let mem m x = M.mem x !m
   *   let iter f m = M.iter f !m
   *   let fold f m x = M.fold f !m x
   *   let stats m = failwith "unimplemented"
   *   let length m = failwith "unimplemented"
   *   let find_opt m = failwith "unimplemented"
   *   let find_all m = failwith "unimplemented"
   *   let filter_map_inplace m = failwith "unimplemented"
   * end *)
  module Map = Hashtbl.Make(struct 
    type t = t_ let equal = (=) let compare = compare let hash = hash 
  end)
  let emptymap name size =
    let m = Map.create size in
    (* at_exit (fun () ->
     *     Printf.printf "MAP %s:" name;
     *     let s = (Map.stats m).Hashtbl.bucket_histogram in
     *     Array.iter (Printf.printf " %i") s;
     *     Printf.printf "\n%!"
     *   ); *)
    m
end


module AVL = Extend(struct
  include Set.Make(struct type t = int let compare = compare end)
  let name = "avl"
  let rem = remove
  (* TOTHINK: one cannot use Hashtbl.hash, is there a better option? *)
  let hash x = fold (lxor) x 0
  let rec full acc = function
    | 0 -> acc
    | n -> full (add (n-1) acc) (n-1)
  let full = full empty
  let size x = fold (fun _ n -> n + 1) x 0
  let shift n x = fold (fun i -> add (i+n)) x empty
  let equal' x y =
    try Some (min_elt (union (diff x y) (diff y x)))
    with Not_found -> None
end)


module Dup(M1: T)(M2: T): T = struct 
  type t = M1.t*M2.t
  let name = "dup("^M1.name^","^M2.name^")"
  let ret ?(msg="") x1 x2 = 
    let x2' = M2.to_list x2 in
    if x2'=[] && not (x2=M2.empty) then failwith "Set.Dup: invalid empty set - %s" msg
    else if M1.to_list x1 <> x2' then failwith "Set.Dup: set difference - %s" msg
    else (x1,x2)
  let ret' ?(msg="") x1 x2 = if x1 = x2 then x1 else failwith "Set.Dup: difference - %s" msg
  let empty = ret ~msg:"empty"  M1.empty M2.empty
  let union (x1,x2) (y1,y2) = ret ~msg:"union" (M1.union x1 y1) (M2.union x2 y2)
  let inter (x1,x2) (y1,y2) = ret ~msg:"inter" (M1.inter x1 y1) (M2.inter x2 y2)
  let singleton x = ret ~msg:"singleton" (M1.singleton x) (M2.singleton x)
  let equal (x1,x2) (y1,y2) = ret' ~msg:"equal" (M1.equal x1 y1) (M2.equal x2 y2)
  let equal' (x1,x2) (y1,y2) = ret' ~msg:"equal'" (M1.equal' x1 y1) (M2.equal' x2 y2)
  let compare (x1,x2) (y1,y2) = M1.compare x1 y1
  let hash (x1,x2) = M1.hash x1
  let mem x (x1,x2) = ret' ~msg:"mem" (M1.mem x x1) (M2.mem x x2)
  let fold f (x1,x2) acc = (* ret' ~msg:"fold" (M1.fold f x1 acc) *) (M2.fold f x2 acc)
  let shift n (x1,x2) = ret ~msg:"shift" (M1.shift n x1) (M2.shift n x2)
  let rem i (x1,x2) = ret ~msg:"rem" (M1.rem i x1) (M2.rem i x2)
  let full n = ret ~msg:"full" (M1.full n) (M2.full n)
  let size (x1,x2) = ret' ~msg:"size" (M1.size x1) (M2.size x2)
  let is_empty (x1,x2) = ret' ~msg:"is_empty" (M1.is_empty x1) (M2.is_empty x2)
  let add i (x1,x2) = ret ~msg:"add" (M1.add i x1) (M2.add i x2)
  let subseteq (x1,x2) (y1,y2) = ret' ~msg:"subseteq" (M1.subseteq x1 y1) (M2.subseteq x2 y2)
  let intersect (x1,x2) (y1,y2) = ret' ~msg:"intersect" (M1.intersect x1 y1) (M2.intersect x2 y2)
  let diff (x1,x2) (y1,y2) = ret ~msg:"diff" (M1.diff x1 y1) (M2.diff x2 y2)
  let set_compare (x1,x2) (y1,y2) = ret' ~msg:"set_compare" (M1.set_compare x1 y1) (M2.set_compare x2 y2)
  let of_list l = ret ~msg:"of_list" (M1.of_list l) (M2.of_list l)
  let to_list (x1,x2) = ret' ~msg:"to_list" (M1.to_list x1) (M2.to_list x2)
  let elements = to_list
  let filter f (x1,x2) = ret ~msg:"filter" (M1.filter f x1) (M2.filter f x2)
  let map f (x1,x2) = ret ~msg:"map" (M1.map f x1) (M2.map f x2)
  let iter f (x1,_) = M1.iter f x1
  let print f (x1,_) = M1.print f x1
  let print' g f (x1,_) = M1.print' g f x1
  let random n p = let x1 = M1.random n p in x1, M2.of_list (M1.to_list x1)
  let random' n =  let x1 = M1.random' n in x1, M2.of_list (M1.to_list x1)
  let forall (x1,x2) f = ret' ~msg:"forall" (M1.forall x1 f) (M2.forall x2 f)
  let exists (x1,x2) f = ret' ~msg:"exists" (M1.exists x1 f) (M2.exists x2 f)
  module Map =
    Hashtbl.Make(struct
        type t = M1.t*M2.t
        let equal (x1,x2) (y1,y2) = ret' (M1.equal x1 y1) (M2.equal x2 y2)
        let compare (x1,x2) (y1,y2) = ret' (M1.compare x1 y1) (M2.compare x2 y2)
        let hash (x1,x2) = Hashtbl.hash (M1.hash x1, M2.hash x2)
      end)
  let emptymap _ size = Map.create size
end
