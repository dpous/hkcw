(* equivalence/inclusion algorithms *)

module NAIVE_BFS: Equivalence.T
module HK_BFS: Equivalence.T
module HKCRW_BFS: Equivalence.T
module HKCTRW_BFS: Equivalence.T
module HKCRW'_BFS: Equivalence.T
module HKCTRW'_BFS: Equivalence.T

module NAIVE_DFS: Equivalence.T
module HK_DFS: Equivalence.T
module HKCRW_DFS: Equivalence.T
module HKCTRW_DFS: Equivalence.T
module HKCRW'_DFS: Equivalence.T
module HKCTRW'_DFS: Equivalence.T

