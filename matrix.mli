open Common

type t

val init: set array -> set -> t
   
val id: int -> t
val mult: t -> t -> t
  
val set: t -> set

val print: out_channel -> t -> unit
