open Common

module UF': UPTO = struct
  let name = "uf'"
  type t = set setmap
  let create ?stats n = Set.emptymap name n
  let unify_eq m =
    let find x = Set.Map.find_opt m x in
    let rec repr x =
      match find x with None -> x | Some y -> 
      match find y with None -> y | Some z -> 
      Set.Map.replace m x z; repr z
    in
    fun p q ->
    let p = repr p in
    let q = repr q in
    (* WARNING: cannot use physical equality test below *)    
    match Set.compare p q with
    | 0 -> true                                
    | 1 -> Set.Map.replace m p q; false (* TODO: fix choice? *)
    | _ -> Set.Map.replace m q p; false
  let unify_incl m x y = unify_eq m (Set.union x y) y
  let unify ~incl = if incl then unify_incl else unify_eq
  let record ?stats _ = ()
end

module UF: UPTO = struct
  let name = "uf"
  type c = L of r | R
  and r = c ref 
  type t = r setmap
  let create ?stats n = Set.emptymap name n
  let rec repr (x: r): r =
    match !x with
    | R -> x
    | L y -> match !y with
             | R -> y
             | L z -> x := L z; repr z
  let repr m x =
    try repr (Set.Map.find m x)
    with Not_found -> 
      let r = ref R in
      Set.Map.add m x r;
      r
  let unify_eq m p q =
    let rp = repr m p in
    let rq = repr m q in
    if rp==rq then true
    else if p<q then (rp:=L rq; false) (* TODO: fix choice? *)
    else (rq:=L rp; false)
  let unify_incl m x y = unify_eq m (Set.union x y) y
  let unify ~incl = if incl then unify_incl else unify_eq
  let record ?stats _ = ()
end

module type TUF = sig
  type 'a t
  val create: (set -> 'a) -> int -> 'a t
  val unify: ('a -> 'a -> 'a * bool) -> 'a t -> set -> set -> bool
  val tag: 'a t -> set -> 'a
  val iter: ('a -> unit) -> 'a t -> unit
end

module TUF: TUF = struct
  type 'a c = L of 'a r | R of 'a
  and 'a r = 'a c ref
  let rec repr (x: 'a r): 'a r * 'a =
    match !x with
    | R v -> x,v
    | L y -> match !y with
             | R v -> y,v
             | L z -> x := L z; repr z
  
  type 'a t = {map: 'a r setmap; init: set -> 'a}
  let repr i m x =
    try repr (Set.Map.find m x)
    with Not_found ->
      let vx = i x in
      let r = ref (R vx) in
      Set.Map.add m x r;
      r,vx
  let create init n = {map=Set.emptymap "TUF" n; init}
  let unify f m p q =
    let rp,v = repr m.init m.map p in
    let rq,w = repr m.init m.map q in
    let r p q = let z,r = f v w in p := L q; q := R z; r in
    if rp==rq then true else if p<q then r rp rq else r rq rp (* TODO: fix choice? *)
  let tag m p = snd (repr m.init m.map p)
  let iter f m =
    Set.Map.iter (fun _ x -> match !x with R a -> f a | _ -> ()) m.map
end

module TUF': TUF = struct
  type 'a r = L of set | R of 'a
  type 'a t = {map: 'a r setmap; init: set -> 'a}
  let repr i m =
    let find x = try Set.Map.find m x with Not_found -> R (i x) in
    let rec repr x =
      match find x with R v -> x,v | L y -> 
      match Set.Map.find m y with R v -> y,v | L z -> 
  	Set.Map.replace m x (L z); repr z
    in repr
  let create init n = {map=Set.emptymap "TUF'" n; init}
  let unify f m p q =
    let rp,v = repr m.init m.map p in
    let rq,w = repr m.init m.map q in
    let r p q = let z,r = f v w in Set.Map.replace m.map p (L q); Set.Map.replace m.map q (R z); r in
    if Set.equal rp rq then true else if p<q then r rp rq else r rq rp (* TODO: fix choice *)
  let tag m p = snd (repr m.init m.map p)
  let iter f m =
    Set.Map.iter (fun _ x -> match x with R a -> f a | _ -> ()) m.map
end
