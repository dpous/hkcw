(* up to congruence technique (ACI)
   based on various algorithms *)

open Common

(** versions not using the todo list *)

(* simple rewriting algorithm *)
module RW: UPTO
(* improved version, with partial decision trees *)
module TRW: UPTO              

(** versions using the todo list *)

(* simple rewriting algorithm *)
module RW'(Q: QUEUE): UPTO'
(* improved version, with partial decision trees *)
module TRW'(Q: QUEUE): UPTO'             
