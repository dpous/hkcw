(* simple program exploiting the library *)

open Common
open Misc
open Equivalence
open Algorithms

(* which upto technique do we use *)
let upto = ref "rw'"

(* which strategy do we use *)
let strategy = ref "bfs"

(* do we prune the NFA *)
let prune = ref false

(* do we use similarity or not *)
let sim = ref false

(* do we print statistics *)
let stats = ref false

let set_upto x =
  let uptos = registered_uptos() in
  if List.mem x uptos then upto := x
  else (
    Printf.eprintf "Unknown up-to technique: %s\nRegistered techniques are: %a\n"
    x (Util.print_list ~sep:", " output_string) uptos;
    exit 1
  )
        
(* running a single instance *)
let run kind a =
  let sim = !sim in
  let upto = !upto in
  let prune = !prune in
  let strategy = !strategy in
  let (_,_,a) as m = match a with
    | `D(f,g) -> Parse.from_files f g
    | `S(f) -> Parse.from_file f
  in
  let h =
    match kind with
    | `Equiv -> equivalent ~upto ~strategy ~prune ~sim
    | `Incl -> included ~upto ~strategy ~prune ~sim
  in
  let _ =
    Printf.printf "parsed automata, %i states\n%!" (NFA.size a);
    match h m with
    | None ->
       Printf.printf "%s\n%!"
         (match kind with `Incl -> "<=" | `Equiv -> "==")
    | Some (l,l') ->
       Printf.printf "%s [%a $ %a]\n%!" 
         (match kind with `Incl -> "</=" | `Equiv -> "=/=") (NFA.print_word a) l (NFA.print_word a) l'
  in
  if !stats then Printf.printf "%a" Stats.print Stats.global

(* parsing command line arguments *)
let rec parse_args = function 
  | [] -> exit 0
  | "-prn"::q -> prune := true; parse_args q
  | "-sim"::q -> sim := true; parse_args q
  | "-bfs"::q -> strategy := "bfs"; parse_args q
  | "-dfs"::q -> strategy := "dfs"; parse_args q
  | "-stat"::q -> stats := true; parse_args q
  | "-upto"::x::q -> set_upto x; parse_args q
  | "-incl"::f::q -> run `Incl (`S(f)); parse_args q
  | "-equiv"::f::q -> run `Equiv (`S(f)); parse_args q
  | "-incl2"::f1::f2::q -> run `Incl (`D(f1,f2)); parse_args q
  | "-equiv2"::f1::f2::q -> run `Equiv (`D(f1,f2)); parse_args q
  | _ -> Printf.printf "usage: %s {[options]* [{-incl,-equiv} file] [{-incl2,-equiv2} file1 file2]}*
options:
 -bfs / -dfs    use the breadth-first (default) of depth-first exploration strategy
 -prn           first prune the NBW
 -sim           reduces the NBW by similarity
 -stat          print statiscics
 -upto [upto]   use the specified upto technique (default: rw')
%!" Sys.argv.(0)

(* main entry point *)
let _ = parse_args (List.tl (Array.to_list Sys.argv))
