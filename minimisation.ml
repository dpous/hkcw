open Common
open Misc
open NFA

(* Brzozowski minimisation *)
let brzozowski a =
  let a = determinise (reverse_nfa a) in
  let n = a.DFA.size in
  determinise (reverse_dfa a), n


(* reducing a NFA up to bisimilarity; we use a naive version of the
   partition refinement algorithm. *)
let bisim_reduce a =
  if a.size < 2 then a,(fun x -> x)
  else if Set.is_empty a.accept then
    {a with size=1;delta=Array.map (fun _ -> [|Set.empty|]) a.delta;accept=Set.empty},(fun _ -> 0)
  else
  let n = a.size in
  let letters = letters a in
  let p = Array.make a.size 0 in
  let map = Set.map (Array.get p) in
  let index_of x =
    let r = ref 0 in
    while p.(!r) =/= x do incr r done;
    !r
  in
  let m' = ref 1 in
  let m = ref 2 in
  Set.iter (fun i -> p.(i) <- 1) a.accept;
  while !m' =/= !m do
    m' := !m;
    for v = 0 to letters-1 do
      let dv = a.delta.(v) in
      let t = Array.init !m (fun _ -> Set.Map.create 13) in
      for i = 0 to n-1 do
	let tpi = t.(p.(i)) in
	let xs = map dv.(i) in
	try let l = Set.Map.find tpi xs in l := i::!l
	with Not_found -> Set.Map.add tpi xs (ref [i])
      done;
      Array.iter (fun t -> 
	match Set.Map.fold (fun _ v q -> !v::q) t [] with
	  | [] -> assert false (* TOFIX: fails for automata with only accepting states *)
	  | _::ls -> 
	    List.iter (fun is -> 
	      let c = !m in
	      incr m;
	      List.iter (fun i -> p.(i) <- c) is
	    ) ls
      ) t;
    done
  done;
  let delta = Array.init letters (fun v -> Array.make !m Set.empty) in
  for x = 0 to !m-1 do
    let i = index_of x in
    for v = 0 to letters-1 do
      delta.(v).(x) <- map a.delta.(v).(i)
    done
  done;
  { a with size = !m;
    delta = delta;
    accept = map a.accept;
    print_state = Util.output_int;
  },(Array.get p)

let bisim_reduce' (x,a) =
  let a,f = bisim_reduce a in
  Set.map f x, a
