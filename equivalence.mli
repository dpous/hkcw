open Common

(* equivalence/inclusion check for sets of states in an NBW, using (bi)simulations up to *)

module type T = sig
  (* returns the result and some internal statistics *)
  val check: incl:bool -> ?prune: bool -> ?sim: bool -> ?stats:Stats.collector ->
             set*set*nfa -> (word*word) option
end

module Make(M: UPTO)(Q: QUEUE): T
module Make'(M: UPTO'): T

(* once the above functors have been applied to some parameters, the
   following functions are available *)
val equivalent:
  upto: string -> strategy: string -> ?prune: bool -> ?sim: bool ->
  ?stats:Stats.collector ->
  set*set*nfa -> (word*word) option
val included:
  upto: string -> strategy: string -> ?prune: bool -> ?sim: bool ->
  ?stats:Stats.collector ->
  set*set*nfa -> (word*word) option

(* registered up-to techniques *)
val registered_uptos: unit -> string list
