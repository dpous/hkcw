open Common

type e = Bot | One | Top
                   
let pls x y = match x,y with
  | Top,_ | _,Top -> Top
  | Bot,z | z,Bot -> z
  | One,One -> One
             
let dot x y = match x,y with
  | Bot,_ | _,Bot -> Bot
  | Top,_ | _,Top -> Top
  | One,One -> One
             
let rec sum s f = match s with
  | 0 -> Bot
  | n -> pls (sum (n-1) f) (f (n-1)) 

let rec exists f = function
  | 0 -> false
  | n -> f (n-1) || exists f (n-1) 
       
type t = e array array

let size = Array.length
  
let matrix s f = Array.init s (fun i -> Array.init s (f i))
       
let id s = matrix s (fun i j -> if i===j then One else Bot)

let mult m n =
  let s = size m in
  matrix s (fun i k -> sum s (fun j -> dot m.(i).(j) n.(j).(k)))

let star m =
  let rec loop x =
    let x' = mult x x in
    if x===x' then x else loop x'
  in loop (matrix (size m) (fun i j -> if i===j then pls One m.(i).(j) else m.(i).(j)))

let set m =
  let s = size m in
  let m = star m in
  let rec loop acc = function
    | 0 -> acc
    | n -> let n = n-1 in
           if exists (fun i -> m.(n).(i)=/=Bot && m.(i).(i)===Top) s then
             loop (Set.add n acc) n
           else
             loop acc n
  in loop Set.empty s

let init t o =
  let s = size t in
  matrix s (fun i j -> if Set.mem j t.(i) then
                         if Set.mem j o then Top else One
                       else Bot)

let print3 f = function
  | Bot -> Printf.fprintf f "."
  | One -> Printf.fprintf f "1"
  | Top -> Printf.fprintf f "*"

let print f m =
  let s = size m - 1 in
  for i = 0 to s do
    for j = 0 to s do
      Printf.fprintf f "%a " print3 m.(i).(j)
    done;
    Printf.fprintf f "\n"
  done
