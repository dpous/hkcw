open Common
open Sets

(* pretty-printing automata *)
val print_nfa: out_channel -> nfa -> unit
val print_nfa': out_channel -> set*nfa -> unit
val print_nfa'': out_channel -> set*set*nfa -> unit
val print_nfa_transitions: out_channel -> nfa -> unit 
val print_dfa: out_channel -> dfa -> unit

(* building an NFA from lists of strings
   starting states, transitions, accepting states *)
val nfa_of_lists:
  string list -> (string*string*string) list -> string list -> set*nfa
val nfa_of_lists':
  string list -> string list -> (string*string*string) list -> string list -> set*set*nfa
  
(* building an NFA from lists of non-negative integers directly
   transitions, accepting states *)
val nfa_of_ilists: (int*letter*int) list -> int list -> nfa
  
(* see a DFA as an NFA *)
val nfa_of_dfa: dfa -> set*nfa

(* reverse an NFA *)
val reverse_nfa: set*nfa -> set*nfa

(* reverse a DFA into an NFA *)
val reverse_dfa: dfa -> set*nfa

(* (disjoint) union of two NFA
   the returned sets correspond to the new starting sets 
   if dsl is set to true, then the states are renamed to make the 
   disjoint-union explicit (otherwise states names are kept and can overlap)
*)
val nfa_union: ?dsl: bool -> set*nfa -> set*nfa -> set*set*nfa

(* NFA without states *)
val empty_nfa: nfa
  
(* accessible subsets construction 
   forget state names if anon is set to true
*)
val determinise: ?anon: bool -> set*nfa -> dfa

(* forget information about state names *)
val anonymise_nfa: nfa -> nfa
val anonymise_dfa: dfa -> dfa

(* generate a random NFA with
   - [n] nodes, 
   - [v] letters,
   - transition density [r] (expected outdegree of each node, 
     for each letter, cf. Tabakov&Vardi'05),
   - probability [pa] for a state to be accepting. 

   if [disjoint] is set, then the produced automaton is a disjoint
   union of two automata such that 0 and 1 do not belong to the same
   automaton. 
   
   if [ne] is set then we never get the empty set (not implemented for disjoint automata)
*)
val random_nfa: ?disjoint:bool -> ?ne:bool -> n:int -> v:int -> r:float -> pa:float -> nfa


(* co-accessible states of an nfa *)
val co_accessible: nfa -> set

(* live states of an nfa *)
val live: set*nfa -> set

(* normalise an nfa by removing useless states and letters, and reindexing states *)
val normalise_nfa: set*nfa -> set*nfa
val normalise_nfa': set*set*nfa -> set*set*nfa

(* number of transitions of a NFA *)
val transitions: nfa -> int
  
(* statistics about out-degree of a NFA: min, median, last decile, max *)
val out_degrees: nfa -> int*int*int*int
