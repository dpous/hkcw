(** Simple module to record statistics, by profiling *)
type collector

val global: collector
val create: unit -> collector
   
type t

(** create a new counter with the given name *)
val counter: ?stats:collector -> string -> t

(** increment a counter *)
val incr: ?n:int -> t -> unit

(** increment a counter (without first naming it)*)
val record: ?stats:collector -> string -> int -> unit

(** getting the value of a counter *)
val get: ?stats:collector -> string -> int

(** count the calls to the given function, under the given name *)
val count_calls: ?stats:collector -> string -> ('a -> 'b) -> 'a -> 'b

(** record the time used by the given function call, under the given name *)
val time_call: ?stats:collector -> ?gc:bool -> string -> ('a -> 'b) -> 'a -> 'b

(** getting a recorded time *)
val get_time: ?stats:collector -> string -> float

(** print the status of all counters *)
val print: out_channel -> collector -> unit

(** reset all counters *)
val reset: collector -> unit


(** basic statistic functions *)
val mean: float list -> float
val quantile: float -> float list -> float
val median: float list -> float
val min: float list -> float
val max: float list -> float
