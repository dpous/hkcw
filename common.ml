open Util

(* letters are just integers *)
type letter = int

(* folding through letters, one way or the other *)
let fold_letters letters f =
  let rec aux i x = if i = letters then x else aux (i+1) (f i x) in
  aux 0 
let fold_letters' letters f =
  let rec aux i x = if i = -1 then x else aux (i-1) (f i x) in
  aux (letters-1)
let iter_letters letters f =
  let rec aux i = if i < letters then (f i; aux (i+1)) in
  aux 0 
let forall_letters letters f =
  let rec aux i = i = letters || (f i && aux (i+1)) in
  aux 0 

(* words (counter-examples) *)
module Word = struct
  type t = letter list
  let compare = compare
  let print letter_name = Util.print_list ~sep:"." (fun f a -> output_string f (letter_name a))
end
module WMap=Map.Make(Word)
module WSet=Set.Make(Word)
type word=Word.t


module Set = Sets.IntSparseList
type set = Set.t
type 'a setmap = 'a Set.Map.t

module IntMap = Map.Make(struct type t = int let compare = compare end)


(* make uses of polymorphic equality explicit *)
let (===) = (=)
let (=/=) = (<>)
let (=) = ()
let (<>) = ()
        

(* abstract queues (for selecting heuristics) *)
module type QUEUE = sig
  val name: string
  type 'a t
  val empty: 'a t
  val push: 'a t -> 'a -> 'a t
  val pop: 'a t -> ('a * 'a t) option
  val filter: ('a -> bool) -> 'a t -> 'a t
  val fold: ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
end

(* shortcuts for comparison types *)
type le = [`Lt|`Eq|`Le]
type ge = [`Gt|`Eq|`Ge]


(* type for DFA *)
module DFA = struct 
  type t = {
    size: int;
    start: int;
    delta: int array array;
    accept: set;
    print_state: out_channel -> int -> unit;
    letter_name: int -> string }
  let letters a = Array.length a.delta
  let size a = a.size
end
type dfa = DFA.t

(* type for NFA *)
module NFA = struct
  type t = {
    size: int;
    delta: set array array;
    accept: set;
    print_state: out_channel -> int -> unit;
    letter_name: int -> string }
  let letters a = Array.length a.delta
  let size a = a.size
  let size' (_,a) = a.size
  let size'' (_,_,a) = a.size
  let delta a v x = a.delta.(v).(x)
  let delta_set a v =
    let d = a.delta.(v) in 
    fun x -> Set.fold (fun i -> Set.union d.(i)) x Set.empty
  let rec mem (x,a) = function
    | [] -> Set.intersect x a.accept
    | b::q -> mem (delta_set a b x,a) q
  let print_letter a f x = Printf.fprintf f "%s" (a.letter_name x)
  let print_word a = Util.print_list ~sep:"." (print_letter a)
end
type nfa = NFA.t

(* type of recorded statistics:
   1. number of pairs whose span is processed
   2. total number of iterations 
*)
type stat = int * int
let sum_stats (a,b) (a',b') = (a+b,a'+b')

(* checking an equivalence using a generic inclusion test *)
let equivalent_from_i included (i,j,a) = 
  match included (i,j,a) with
  | None,n -> let r,n' = included (j,i,a) in r,sum_stats n n'
  | rn -> rn
          
(* checking an inclusion using a generic equivalence test *)
let included_from_e equivalent (i,j,a) = equivalent (Set.union i j,j,a)

        
(* abstract signature for up-to techniques not taking the todo list into account *)
module type UPTO = sig
  type t
  (* create a new unifier; the argument is the number of states *)
  val create: ?stats:Stats.collector -> int -> t
  (* test whether two sets are already known to be equivalent, and unify them 
     reason up to symmetry iff [incl] is true *)
  val unify: incl:bool -> t -> set -> set -> bool
  (* record statistics *)
  val record: ?stats:Stats.collector -> t -> unit
  (* name of the algorithm *)
  val name: string
end

(* abstract signature for up-to techniques taking the todo list into account *)
module type UPTO' = sig
  type id
  type t
  (* create a new unifier; the argument is the number of states *)
  val create: ?stats:Stats.collector -> int -> t
  (* push a pair of sets to be checked later, with a given potential counter-example *)
  val push: incl:bool -> t -> letter list -> set -> set -> unit
  (* get a pair of sets to be processed (together with the unique id of the pair) *)
  val pop: t -> (id*letter list*set*set) option
  (* test whether the given pair should be skipped (the id and the pair of sets should match) *)
  val skip: incl:bool -> t -> id -> set -> set -> bool
  (* record statistics *)
  val record: ?stats:Stats.collector -> t -> unit
  (* name of the algorithm and strategy *)
  val name: string
  val strategy: string
end

(* what to debug *)
let debug _ = false
let debug = function `BLoop -> true | _ -> false

(* upto technique duplicators, to check implementations *)
module Dup(M1: UPTO)(M2: UPTO): UPTO = struct
  type t = M1.t*M2.t
  let create ?stats n =
    M1.create ?stats n, M2.create ?stats n
  let unify ~incl (u1,u2) x y =
    let r1, r2 = M1.unify ~incl u1 x y, M2.unify ~incl u2 x y in
    if r1=/=r2 then (flush stdout; failwith "Upto techniques %s and %s do not match" M1.name M2.name);
    r1
  let record ?stats (u1,u2) = M1.record ?stats u1; M2.record ?stats u2
  let name = "dup("^M1.name^","^M2.name^")"
end

module Trace(M: UPTO): UPTO = struct
  type t = M.t
  let create ?stats n =
    Printf.printf "create %i\n" n;
    M.create ?stats n
  let unify ~incl u x y =
    Printf.printf "unify %b %a %a: " incl Set.print x Set.print y;
    let r = M.unify ~incl u x y in
    Printf.printf "%b\n%!" r;
    r
  let record ?stats u = M.record ?stats u
  let name = "trace("^M.name^")"
end

module Dup'(M1: UPTO')(M2: UPTO'): UPTO' = struct
  type id = M1.id*M2.id
  type t = M1.t*M2.t
  let create ?stats n = 
    if debug `Dup' then Printf.printf "create %i\n" n;
    M1.create ?stats n, M2.create ?stats n
  let skip ~incl (u1,u2) (i1,i2) x y =
    if debug `Dup' then Printf.printf "skip %b %a %a\n" incl Set.print x Set.print y; 
    let r1, r2 = M1.skip ~incl u1 i1 x y, M2.skip ~incl u2 i2 x y in
    if debug `Dup' then Printf.printf "(%b, %b)\n" r1 r2;
    if r1=/=r2 then failwith "Upto techniques %s and %s do not match (skip)" M1.name M2.name;
    r1
  let push ~incl (u1,u2) w x y =
    if debug `Dup' then Printf.printf "push %b %a %a\n" incl Set.print x Set.print y;
    M1.push ~incl u1 w x y;
    M2.push ~incl u2 w x y
  let pop (u1,u2) =
    match M1.pop u1, M2.pop u2 with
    | None, None -> None
    | Some (i1,w1,x1,y1), Some (i2,w2,x2,y2) ->
       if w1=/=w2 || not (Set.equal x1 x2 && Set.equal y1 y2) then
         failwith "Upto techniques %s and %s do not match (pop.1)" M1.name M2.name;
       if debug `Dup' then Printf.printf "pop %a %a\n" Set.print x1 Set.print y1;
       Some ((i1,i2),w1,x1,y1)
    | _ -> failwith "Upto techniques %s and %s do not match (pop.2)" M1.name M2.name
  let name = "dup("^M1.name^","^M2.name^")"
  let record ?stats (u1,u2) = M1.record ?stats u1; M2.record ?stats u2
  let strategy = "dup("^M1.strategy^","^M2.strategy^")"
end

module Trace'(M: UPTO'): UPTO' = struct
  type id = M.id
  type t = M.t
  let create ?stats n = 
    Printf.printf "create %i\n" n;
    M.create ?stats n
  let skip ~incl u i x y =
    Printf.printf "skip %b %a %a:" incl Set.print x Set.print y; 
    let r = M.skip ~incl u i x y in
    Printf.printf "%b\n%!" r;
    r
  let push ~incl u w x y =
    Printf.printf "push %b %a %a\n" incl Set.print x Set.print y;
    M.push ~incl u w x y
  let pop u =
    match M.pop u with
    | None -> None
    | Some (i,w,x,y) ->
       Printf.printf "pop %a %a\n" Set.print x Set.print y;
       Some (i,w,x,y)
  let name = "trace("^M.name^")"
  let record ?stats u = M.record ?stats u
  let strategy = "trace("^M.strategy^")"
end

(* turn an upto technique not using todo into one pretending it does *)
module Extend(M: UPTO)(Q: QUEUE): UPTO' = struct
  let name = M.name
  let strategy = Q.name
  type id = unit
  type t = {t: M.t; mutable q: (unit*letter list*set*set) Q.t}
  let create ?stats n = {t=M.create ?stats n; q=Q.empty}
  let skip ~incl m i x y = M.unify ~incl m.t x y
  let push ~incl m w x y = m.q <- Q.push m.q ((),w,x,y)
  let pop m = match Q.pop m.q with Some(r,q) -> m.q<-q; Some r | None -> None
  let record ?stats m = M.record ?stats m.t
end

(* Stores: a list of processed elements together with a queue of elements to be processed *)
module MakeStore(Queue: QUEUE): sig
  type 'a t
  (* empty store *)
  val empty: 'a t
  (* pick an element from `todo' and move it to `processed' *)
  val shift: 'a t -> (letter list * 'a * 'a t) option
  (* pick an element from `todo' without moving it to `processed' *)
  val pick: 'a t -> (letter list * 'a * 'a t) option
  (* push an element into `todo' *)
  val push: letter list -> 'a -> 'a t -> 'a t
  (* push an element into `processed *)
  val push_done: 'a -> 'a t -> 'a t
  (* filter the store according to the given function *)
  val filter: ('a -> bool) -> 'a t -> 'a t
  (* fold through all elements of the store *)
  val fold: ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  (* number of processed elements in the store *)
  val processed_length: 'a t -> int
end = struct
  type 'a t = {
    processed: 'a list;
    todo: (letter list * 'a) Queue.t;
  }
  let empty = {processed = []; todo = Queue.empty}
  let shift s =
    match Queue.pop s.todo with
      | None -> None
      | Some ((w,x),todo) -> Some (w,x,{processed=x::s.processed; todo=todo})
  let pick s =
    match Queue.pop s.todo with
      | None -> None
      | Some ((w,x),todo) -> Some (w,x,{s with todo=todo})
  let push w x s = {s with todo=Queue.push s.todo (w,x)}
  let push_done x s = {s with processed=x::s.processed}
  let filter f s = {processed=filter_rev f s.processed; todo=Queue.filter (fun (_,x) -> f x) s.todo}
  let fold f s a = fold f s.processed (Queue.fold (fun (_,x) o -> f x o) s.todo a)
  let processed_length s = List.length s.processed
end

let obs m f x =
  Printf.printf "%s(\n%!" m;
  let r = f x in
  Printf.printf ")%s\n%!" m;
  r
