open Common

(* printing NFA in the vata format 
   if compact is set to false, do not compact lines 
   (to really adhere to vata format) *)
val print: ?compact:bool -> out_channel -> set*nfa -> unit

(* reading NFA in the same format from a file *)
val from_file: string -> set*set*nfa

(* reading two NFA and returning their disjoint union *)
val from_files: string -> string -> set*set*nfa
