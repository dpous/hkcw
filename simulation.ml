open Common
open NFA

(* redundant representation *)
type t = {
  rel: bool array array; (* rel.(i).(j) holds if i<=j *)
  dn: set array; (* [dn.(i)] is the set of states smaller than [i] (except [i]) *)
  up: set array; (* [up.(i)] is the set of states larger than [i] (except [i]) *)
}

let print f r = Array.iteri (fun i s -> if not (Set.is_empty s) then
    Printf.fprintf f "%a <= %i\n" Set.print s i) r.dn

let exists' x f =
  try Array.fold_right (fun i _ -> f i && raise Not_found) x false
  with Not_found -> true

let iter n f =
  let rec aux n = if n<0 then () else (f n; aux (n-1)) in
  aux (n-1)

let forall n f =
  let rec aux n = n<0 || (f n && aux (n-1)) in
  aux (n-1)

let of_fun n f =
  let rec line i j = 
    if i===n then Set.empty 
    else if i=/=j && f i j then Set.add i (line (i+1) j)
    else line (i+1) j
  in
  Array.init n (line 0)

let of_rel n rel = {
  rel=rel;
  dn=of_fun n (fun i j -> rel.(i).(j));
  up=of_fun n (fun i j -> rel.(j).(i)) }


(* very naive algorithm to compute the largest simulation *)

let largest_naive a =
  let n = a.size in
  let rel = 
    Array.init n (fun i -> 
      Array.init n (fun j -> 
        Set.mem i a.accept <= Set.mem j a.accept)) 
  in
  let rec loop () =
    let changed = ref false in
    for i=0 to n-1 do
      for j=0 to n-1 do
        if rel.(i).(j) && 
          exists' a.delta (fun d ->
          Set.exists d.(i) (fun i' -> 
          Set.forall d.(j) (fun j' -> 
          not rel.(i').(j'))))
        then (
          changed := true;
          rel.(i).(j) <- false
        )  
      done
    done;
    if !changed then loop() 
    else of_rel n rel 
  in loop()


(* algorithm proposed by M. Hezinger, T. Henzinger, P. Kopke in
   "Computing Simulations on Finite and Infinite Graphs", 
   in Proc. FOCS'95 *)
exception Found of letter * int * set
let largest_focs95_buggy a =
  let _,a' = Misc.reverse_nfa (Set.empty,a) in
  let pst = delta a in
  let pre = delta a' in
  (* let pst_set = delta_set a in *)
  let pre_set = delta_set a' in
  let size = a.size in
  let all_states = Set.full size in
  let letters = letters a in
  let prevsim = Array.make size all_states in
  let sim = 
    Array.init size (fun v -> 
      Set.filter (fun u ->
        forall letters (fun i -> 
          (Set.is_empty (pst i v)) || not (Set.is_empty (pst i u)))
      ) (if Set.mem v a.accept then a.accept else all_states))
  in
  let remove = 
    Array.init letters (fun i -> 
      let p = pre_set i all_states in
      Array.init size (fun v -> 
        Set.diff p (pre_set i sim.(v))
      ))
  in
  let count =
    Array.init letters (fun i -> 
      Array.init size (fun w -> 
        let p = pst i w in
        Array.init size (fun u -> 
          Set.size (Set.inter p sim.(u))
        )))
  in
  let rec loop n =
    (* Printf.printf "loop %i\n%!" n; *)
    (* iter size (fun i -> Printf.printf "remove (%i)=%a\n" i Set.print remove.(0).(i)); *)
    (* iter size (fun i -> Printf.printf "prevsim(%i)=%a\n" i Set.print prevsim.(i)); *)
    (* iter size (fun i -> Printf.printf "    sim(%i)=%a\n" i Set.print sim.(i)); *)
    try 
      Array.iteri (fun i -> 
        Array.iteri (fun v rem ->
          if not (Set.is_empty rem) then 
            raise (Found(i,v,rem))
        )) remove
    with Found(i,v,rem) ->
      assert(
        forall size (fun v -> Set.subseteq sim.(v) prevsim.(v)));
      assert(
        forall letters (fun i -> 
          forall size (fun u ->
            forall size (fun v -> 
              not (Set.mem v (pst i u)) || Set.subseteq sim.(u) (pre_set i prevsim.(v))
            ))));
      assert(
        forall letters (fun i -> 
          forall size (fun v -> 
            Set.equal remove.(i).(v) (Set.diff (pre_set i prevsim.(v)) (pre_set i sim.(v))))));
      (* Printf.printf "cutting with %i\n" v; *)
      prevsim.(v) <- sim.(v);
      remove.(i).(v) <- Set.empty;
      iter letters (fun i -> 
      Set.iter (fun u -> 
        Set.iter (fun w ->
          if Set.mem w sim.(u) then (
            sim.(u) <- Set.rem w sim.(u); (* [sim.(u) <- Set.diff sim.(u) riv] avant la boucle ? *)
            Set.iter (fun w'' ->
              count.(i).(w'').(u) <- count.(i).(w'').(u)-1;
                (* assert ((count.(j).(w'').(u) = 0) = not (Set.intersect (pst j w'') sim.(u))); *)
              if count.(i).(w'').(u) === 0 then
                remove.(i).(u) <- Set.add w'' remove.(i).(u)
            ) ((* Set.diff *) (pre i w) (* (pre_set j sim.(u)) *)))
        ) rem
      ) (pre i v));
      loop (n+1)
  in 
  loop 0;
  let rel = 
    Array.init size (fun u -> 
      Array.init size (fun v -> 
        Set.mem v sim.(u)))
  in
  iter size (fun u -> sim.(u) <- Set.rem u sim.(u));
  { rel; dn=of_fun size (fun i j -> rel.(i).(j)); up=sim; }



(* intermediate algorithm proposed by M. Hezinger, T. Henzinger, P. Kopke in
   "Computing Simulations on Finite and Infinite Graphs", 
   in Proc. FOCS'95 *)
exception IFound of int
let largest_focs95_intermediate a =
  (* Printf.printf "s%!"; *)
  let _,a' = Misc.reverse_nfa (Set.empty,a) in
  (* Printf.printf "s%!"; *)
  let pst = delta a in
  let pre = delta a' in
  (* let pst_set = delta_set a in *)
  let pre_set = delta_set a' in
  let size = a.size in
  let all_states = Set.full size in
  let letters = letters a in
  let prevsim = Array.make size all_states in
  let sim = 
    Array.init size (fun v -> 
      Set.filter (fun u ->
        forall letters (fun i -> 
          (Set.is_empty (pst i v)) || not (Set.is_empty (pst i u)))
      ) (if Set.mem v a.accept then a.accept else all_states))
  in
  let rec loop n =
    try iter size (fun v -> if not (Set.equal sim.(v) prevsim.(v)) then raise (IFound v))
    with IFound(v) ->
      let psv = prevsim.(v) in
      prevsim.(v) <- sim.(v);
      iter letters (fun i ->
        let rem = Set.diff (pre_set i psv) (pre_set i sim.(v)) in
        Set.iter (fun u -> 
          sim.(u) <- Set.diff sim.(u) rem
        ) (pre i v));
      loop (n+1)
  in 
  loop 0;
  let rel = 
    Array.init size (fun u -> 
      Array.init size (fun v -> 
        Set.mem v sim.(u)))
  in
  iter size (fun u -> sim.(u) <- Set.rem u sim.(u));
  { rel; dn=of_fun size (fun i j -> rel.(i).(j)); up=sim; }

let largest_focs95_attempt a =
  let _,a' = Misc.reverse_nfa (Set.empty,a) in
  let pst = delta a in
  let pre = delta a' in
  (* let pst_set = delta_set a in *)
  let pre_set = delta_set a' in
  let size = a.size in
  let all_states = Set.full size in
  let letters = letters a in
  let sim = 
    Array.init size (fun v -> 
      Set.filter (fun u ->
        forall letters (fun i -> 
          (Set.is_empty (pst i v)) || not (Set.is_empty (pst i u)))
      ) (if Set.mem v a.accept then a.accept else all_states))
  in
  let remove = Array.map (Set.diff all_states) sim in
  let rec loop n =
    try iter size (fun v -> if not (Set.is_empty remove.(v)) then raise (IFound v))
    with IFound(v) ->
      let rem = remove.(v) in
      remove.(v) <- Set.empty;
      iter letters (fun i ->
        let rem = Set.diff (pre_set i rem) (pre_set i sim.(v)) in
        Set.iter (fun u -> 
          Set.iter (fun w -> 
            if Set.mem w sim.(u) then (
              sim.(u) <- Set.rem w sim.(u);
              remove.(u) <- Set.union (Set.diff (pre i w) (pre_set i sim.(u))) remove.(u))
          ) rem
        ) (pre i v));
      loop (n+1)
  in 
  loop 0;
  let rel = 
    Array.init size (fun u -> 
      Array.init size (fun v -> 
        Set.mem v sim.(u)))
  in
  iter size (fun u -> sim.(u) <- Set.rem u sim.(u));
  { rel; dn=of_fun size (fun i j -> rel.(i).(j)); up=sim; }



(* libvata implementation (be careful, the bridge is highly buggy) *)
let largest_libvata a = failwith "DISABLE: largest_lib_vata" (*
  let to_bool = function
    | '1' -> true
    | '0' -> false
    | _ -> failwith "illegal character in libvata simulation output"
  in
  let f,o = Filename.open_temp_file "aut" "" in
  Timbuk.print o ("A",Set.full a.size,a);
  close_out o;
  let i = Printf.kprintf Unix.open_process_in "%s sim -s -o dir=up %s" Bridge.vata f in
  let rel = Array.init a.size (fun _ ->
    let l = input_line i in
    Array.init a.size (fun j -> to_bool l.[j]))
  in
  if Unix.close_process_in i =/= Unix.WEXITED 0 then failwith "libvata returned an error while computing similarity";
  Sys.remove f;
  of_rel a.size rel *)





(* additional functions *)

let saturate r x = 
  Set.fold (fun i -> Set.union r.dn.(i)) x x

let saturate_nfa r t = 
  { t with delta = Array.map (Array.map (saturate r)) t.delta }
let saturate_nfa' r (x,t) = saturate r x, saturate_nfa r t
let saturate_nfa'' r (x,y,t) = saturate r x, saturate r y, saturate_nfa r t

let reduce_nfa' r (x,y,a) =
  let d = ref 0 in
  let t = Array.init a.size (fun i -> 
              match Set.to_list (Set.inter r.up.(i) r.dn.(i)) 
              with j::_ when j<i -> incr d; j,!d | _ -> i,!d)
  in
  if !d===0 then r,(x,y,a) else
    (* Printf.printf "removed %i states\n%!" !d; *)
    let size = a.size - !d in
    let letters = letters a in
    let f = let t =
              (Array.init a.size (fun i -> 
                   let j,_ = t.(i) in
                   let _,d = t.(j) in
                   j-d))
	    in fun i -> 
	       assert (i<a.size);
	       assert (t.(i)<size);
	       t.(i)
    in
    let delta = Array.make_matrix letters size Set.empty in
    for v = 0 to letters-1 do
      for i = 0 to a.size-1 do
        delta.(v).(f i) <- Set.map f (a.delta.(v).(i))
      done
    done;
    let rel = Array.make_matrix size size false in
    for i = 0 to a.size-1 do
      for j = 0 to a.size-1 do
        rel.(f i).(f j) <- r.rel.(i).(j);
        assert (f i === f j || (not r.rel.(i).(j)) || (not r.rel.(j).(i)))
      done
    done;
    of_rel size rel,
    (Set.map f x, Set.map f y,
     { size; delta;
       accept = Set.map f a.accept;
       letter_name=a.letter_name;
       print_state=Util.output_int; (* TODO: trace names *)
    })
let reduce_nfa r (x,a) = let (r,(x,_,a)) = reduce_nfa' r (x,Set.empty,a) in r,(x,a)

let minimise r x = 
  Set.fold (fun i acc -> if Set.intersect r.up.(i) acc then Set.rem i acc else acc) x x

let lower r x y = r.rel.(x).(y)

let compare r x y =
  let rel = r.rel in
  match rel.(x).(y), rel.(y).(x) with
    | true,true -> `Eq
    | true,false -> `Lt
    | false,true -> `Gt
    | false,false -> `N
  
let set_lower r x y =
  let rel = r.rel in
  Set.forall x (fun i ->
  Set.exists y (fun j -> 
  rel.(i).(j)))

let set_compare r x y = 
  let x = saturate r x in
  let y = saturate r y in
  Set.set_compare x y      

let size r = Array.fold_left (fun acc x -> acc + Set.size x) 0 r.up

let kernel_size r = 
  let c = ref 0 in
  let r = r.rel in
  Array.iteri (fun i -> Array.iteri (fun j b -> if b && i=/=j && r.(j).(i) then incr c)) r;
  !c

let of_list n l =
  let rel = Array.make_matrix n n false in
  List.iter (fun (i,j) -> List.iter (fun i -> rel.(i).(j) <- true) (j::i)) l;
  of_rel n rel  

(* selecting the implementation of [largest] *)
let largest_focs95 = largest_focs95_intermediate
let largest = largest_focs95_intermediate


let fully_reduce_nfa' (_,_,a as m) =
  let t = largest a in
  let t,m = reduce_nfa' t m in
  saturate_nfa'' t m
let fully_reduce_nfa (_,a as m) =
  let t = largest a in
  let t,m = reduce_nfa t m in
  saturate_nfa' t m
