open Common
open NFA
open Util   
open Misc   

module type T = sig
  (* returns the result and some statistics *)
  val check: incl:bool -> ?prune: bool -> ?sim: bool -> ?stats:Stats.collector ->
             set*set*nfa -> (word*word) option
end

let algos = Hashtbl.create 30

exception Found of word

module Make'(M: UPTO') = struct

  let check ~incl ?stats (x,y,a) =
    let letters = letters a in

    (* computation of the matrices of interest *)
    let a' = Array.mapi (fun i t -> i,Matrix.init t a.accept) a.delta in
    let mxs = Hashtbl.create (a.size*a.size) in
    let rec mx_loop = function
      | [] -> ()
      | (w,m)::todo ->
         if Hashtbl.mem mxs m then mx_loop todo else (
           Hashtbl.add mxs m w;
           mx_loop (Array.fold_right
                      (fun (i,mi) todo -> (i::w, Matrix.mult m mi) :: todo) a' todo)
         )         
    in
    Stats.time_call ?stats ~gc:false "matrix loop"
      mx_loop [[],Matrix.id a.size];
    Stats.record ?stats "size of Büchi transition monoid" (Hashtbl.length mxs);
    let vmxs = Set.emptymap "vmx" (Hashtbl.length mxs) in
    Stats.time_call ?stats ~gc:false "computation of lassos"
      (Hashtbl.iter (fun m w ->
           let v = Matrix.set m in
           if debug `BLoop then
             Printf.printf "matrix of '%a', associated set %a:\n%a" (print_word a) (List.rev w) Set.print v Matrix.print m;
           Set.Map.replace vmxs v w
      )) mxs;
    (* NOTE: above one could choose the shortest word *)
    Stats.record ?stats "relevant Büchi sets" (Set.Map.length vmxs);
    let vmxs = Set.Map.fold (fun x w l -> (w,x)::l) vmxs [] in
    (* vmxs now contains the list of matrices to be used to check outputs *)

    (* computation of the bisimulation upto *)
    let pairs = Stats.counter ?stats "pairs" in
    let candidate = M.create ?stats a.size in
    let push_span w x y =
      Stats.incr pairs;
      iter_letters letters (fun v -> 
          let dset = delta_set a v in
	  M.push ~incl candidate (v::w) (dset x) (dset y))
    in
    let check f =
      try List.iter (fun (w,x) -> if f x then raise (Found w)) vmxs; None
      with Found w -> Some (List.rev w)
    in
    let bad_outputs x y =
      if incl then check (fun m -> Set.intersect m x && not (Set.intersect m y))
      else check (fun m -> Set.intersect m x =/= Set.intersect m y)
    in
    let rec prf_loop () = 
      match M.pop candidate with
      | None -> 
         if debug `BLoop then Printf.printf "success\n\n%!";
         None
      | Some (i,w,x,y) ->
         (* if pruned && (Set.is_empty x =/= Set.is_empty y) then Some (List.rev w) else *)
	 match bad_outputs x y with
         | Some w' -> 
            if debug `BLoop then
              Printf.printf " %a =/= %a : %a(%a)^w\n\n%!"
                Set.print x Set.print y (print_word a) (List.rev w) (print_word a) w';
            Some (List.rev w, w')
         | None ->
            if M.skip ~incl candidate i x y then (
              if debug `BLoop then Printf.printf " %a ... %a\n" Set.print x Set.print y;
              prf_loop ()
            ) else (
              if debug `BLoop then Printf.printf " %a --- %a\n" Set.print x Set.print y;
              push_span w x y;
              prf_loop ()
            )
    in
    M.push ~incl candidate [] x y;
    if debug `BLoop then Printf.printf "starting prefix loop\n";
    let r = Stats.time_call ?stats ~gc:false "prefix loop" prf_loop () in
    M.record ?stats candidate;
    r

  let record_diff ?stats s f (_,_,a as m) =
    let _,_,a' as m = Stats.time_call ?stats s f m in
    Stats.record ?stats s (a'.NFA.size - a.NFA.size);
    m

  let check ~incl ?(prune=false) ?(sim=false) ?stats m =
    let m = if prune then record_diff ?stats "pruned" normalise_nfa' m else m in
    let m = if sim then record_diff ?stats "reduced" Simulation.fully_reduce_nfa' m
            else m
    in
    check ~incl ?stats m    

  let _ = Hashtbl.add algos (M.name,M.strategy) check

end

module Make(M: UPTO)(Q: QUEUE) = Make'(Extend(M)(Q))

let equivalent ~upto ~strategy =
  try Hashtbl.find algos (upto,strategy) ~incl:false
  with Not_found -> failwith "unknown/unregistered algorithm (%s,%s)" upto strategy

let included ~upto ~strategy =
  try Hashtbl.find algos (upto,strategy) ~incl:true
  with Not_found -> failwith "unknown/unregistered algorithm (%s,%s)" upto strategy

let registered_uptos () =
  let rec insert x = function
    | [] -> [x]
    | y::q as l ->
       match compare x y with
       | -1 -> x::l
       | 0 -> l
       | _ -> y::insert x q
  in
  Hashtbl.fold (fun (x,_) _ acc -> insert x acc) algos [] 
