{ open Parser }

let letter = ['a'-'z' 'A'-'Z' '0'-'9' '_' '\'' '.']
let blank = [ ' ' '\r' '\t' ]

rule token = parse
  | blank           { token lexbuf }
  | '-'             { DASH }
  | "->"            { TO }
  | (letter+) as s  { ID s }
  | '('             { LPAR }
  | ')'             { RPAR }
  | '\n'            { NL }
  | eof             { EOF }
  | "#SIZE"         { SIZE }
  | "%Initial1"     { INIT1 }
  | "%Initial2"     { INIT2 }
  | "%Initial"      { INIT1 }
  | "%Final"        { FINAL }
  | "@NFA"          { skip lexbuf }
  | "%Name"         { skip lexbuf }
  | "%States"       { skip lexbuf }
  | "%Alphabet"     { skip lexbuf }
  | '#'             { skip lexbuf }
  | _ as c          { Printf.kprintf failwith "lexing error near `%c'" c }

and skip = parse
  | '\n' { NL }
  | _    { skip lexbuf }
