
(* FIFO queues give breadth-first traversal *)
module BFS = struct
  let name = "bfs"
  (* TODO: check Okasaki's book... *)
  type 'a t = 'a list * 'a list
  let empty = [],[]
  let push (h,k) x = h,x::k
  let pop (h,k) = match h with
    | x::h -> Some (x, (h,k))
    | [] -> match List.rev k with 
        | [] -> None
        | x::h -> Some (x, (h,[]))
  let filter f = 
    let rec xfilter last = function
      | [] -> last
      | x::q when f x -> x::xfilter last q
      | _::q -> xfilter last q
    in
    fun (h,k) -> xfilter (Util.filter_rev f k) h,[]
  let fold f (h,k) a =  Util.fold f h (Util.fold f k a)
end

(* LIFO queues give depth-first traversal *)
module DFS = struct
  let name = "dfs"
  type 'a t = 'a list
  let empty = []
  let push r x = x :: r
  let pop r = match r with
    | [] -> None
    | x::q -> Some (x,q)
  let filter = List.filter
  let fold = Util.fold
end

(* Random queues for random traversal *)
module RFS = struct
  let name = "rfs"
  include DFS
  let rec get acc = function
    | _,[] -> None
    | 0,x::q -> Some (x,List.rev_append acc q)
    | n,x::q -> get (x::acc) (n-1,q)
  let pop r = get [] (Random.int (List.length r),r)
end
