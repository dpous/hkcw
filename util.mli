(* printing integers *)
val output_int: out_channel -> int -> unit

(* printing lists *)
val print_list: ?sep:string -> (out_channel -> 'a -> unit) -> out_channel -> 'a list -> unit

(* tail-rec [List.fold_left] with the type of [List.fold_right] *)
val fold : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b

(* filtering and reversing a list *)
val filter_rev : ('a -> bool) -> 'a list -> 'a list

(* timing a function call *)
val time : ?gc:bool -> ('a -> 'b) -> 'a -> 'b * float
val print_time : ?gc:bool -> string -> ('a -> 'b) -> 'a -> 'b

(* formatted failwith *)
val failwith : ('a, unit, string, 'b) format4 -> 'a
