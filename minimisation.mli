open Common

(* minimisation using Brzozowski's algorithm; the returned integer is
   the size of the intermediate automaton. *)
val brzozowski: set*nfa -> dfa * int

(* reduction of a NFA up to bisimilarity; the returned function maps
   old states to new states. *)
val bisim_reduce: nfa -> nfa * (int -> int)
val bisim_reduce': set*nfa -> set*nfa
