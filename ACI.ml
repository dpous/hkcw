open Util
open Common

(* simple algorithm, via rewriting into normal forms *)
module RW: UPTO = struct
  let name = "rw"
  type f = (set*set) list
  type t = {mutable rules: f; mutable applied_rules: int}
  let incr_applied_rules m = m.applied_rules <- m.applied_rules+1
  let rec repr m skipped changed p = function
    | [] -> if changed then repr m [] false p skipped else p
    | (l,r) as lr::rules ->
       if Set.subseteq r p then
         repr m skipped changed p rules
       else if Set.subseteq l p then 
         let pr = Set.union p r in
         incr_applied_rules m;
         repr m skipped (changed || not (Set.equal p pr)) pr rules
       else 
	 repr m (lr::skipped) changed p rules
  let repr m rules p = repr m [] false p rules
  let create ?stats _ =
    {rules = []; applied_rules = 0}
    (* TODO: why putting the normalised sets in the LHS? *)
    (* TODO: lazyness? *)
    (* TODO: idem in RW' below *)
  let unify_incl m p q =
    let q = repr m m.rules q in
    if Set.subseteq p q then true
    else (m.rules <- (q,Set.union p q)::m.rules; false)
  let unify_eq m p q =
      let p = repr m m.rules p in
      let q = repr m m.rules q in
      if Set.equal p q then true else 
        let pq = Set.union p q in
        if Set.equal p pq then m.rules <- (q,pq)::m.rules
        else if Set.equal q pq then m.rules <- (p,pq)::m.rules
        else m.rules <- (p,pq)::(q,pq)::m.rules;
        false
  let unify ~incl = if incl then unify_incl else unify_eq
  let record ?stats m =
    Stats.record ?stats "final number of rules" (List.length m.rules);
    Stats.record ?stats "applied rules" m.applied_rules
end

module RW'(Q: QUEUE): UPTO' = struct
  let name = "rw'"
  let strategy = Q.name
  module Store = MakeStore(Q)
  type id = unit
  type t = {
      mutable store: (set*set) Store.t;
      applied_rules: Stats.t;
    }
  let rstep ~incl m (l,r as lr) (skipped,p) =
    if Set.subseteq r p then (Stats.incr m.applied_rules; skipped, Set.union p l) else
    if not incl && Set.subseteq l p then (Stats.incr m.applied_rules; skipped, Set.union p r) else 
    lr::skipped, p
  let rec xrepr ~incl m rules p =
    let skipped,p' = fold (rstep ~incl m) rules ([],p) in
    if Set.equal p p' then p else xrepr ~incl m skipped p'
  let repr ~incl m s p =
    let skipped,p' = Store.fold (rstep ~incl m) s ([],p) in
    if Set.equal p p' then p else xrepr ~incl m skipped p'
  let in_closure ~incl m x y =
    if incl then Set.subseteq x (repr ~incl:true m m.store y)
    else Set.equal (repr ~incl:false m m.store x) (repr ~incl:false m m.store y)
  let create ?stats _ = {store=Store.empty; applied_rules=Stats.counter ?stats "applied rules"}
  let push ~incl m w x y = m.store <- Store.push w (x,y) m.store
  let pop m =
    match Store.pick m.store with
    | Some(w,(x,y),s') -> m.store<-s'; Some ((),w,x,y)
    | None -> None
  let skip ~incl m _ x y =
    if in_closure ~incl m x y then true
    else (m.store<-Store.push_done (x,y) m.store; false)
  let record ?stats m =
    Stats.record ?stats "final number of rules" (Store.processed_length m.store)
end
         
(* modules for improving the above algorithms by the way rewriting rules are stored and refined *)
module type RULES = sig
  (* representing a set of rewriting rules to perform efficient congruence tests.
     this module provides only the "one-pass" rewriting process *)
  type t
  val empty: t
  val add: set -> set -> t -> t	(* adding a rewriting rule to the candidate *)
  val pass: t -> set -> t * set	(* a single parallel rewriting pass *)
  val record: ?stats:Stats.collector -> t -> unit
end

module SR: RULES = struct
  (* simple candidates as lists of pairs, rewritten from left to right *)
  type t = (set*set) list
  let empty = []
  let pass rules x = List.fold_left (fun (rules,z) (x,y as xy) ->
    if Set.subseteq x z then rules, Set.union y z else
      xy::rules,z
  ) ([],x) rules
  let add x y rules = (x,y) :: rules
  let record ?stats t = Stats.record ?stats "final number of rules" (List.length t)
end

module LR: RULES = struct
  (* candidates as lists of pairs, rewritten from left to right ; 
     the candidate is simplified a little bit when adding new pairs *)
  type t = (set*set) list
  let empty = []
  let pass rules x = List.fold_left (fun (rules,z) (x,y as xy) ->
    if Set.subseteq x z then rules, Set.union y z else
      xy::rules,z
  ) ([],x) rules
  let add x x' =
    let upd z' = if Set.subseteq x z' then Set.union x' z' else z' in
    let rec xadd' = function
      | [] -> []
      | (z,z')::q -> (z,upd z')::xadd' q
    in
    let rec xadd = function
      | [] -> [x,x']
      | (z,z')::q -> match compare x z with
	  | 1 -> (z,upd z')::xadd q
	  | 0 -> (z,Set.union x' z')::xadd' q
	  | _ -> (x,x')::xadd' q
    in
    xadd
  let record ?stats _ = ()
  let record ?stats t = Stats.record ?stats "final number of rules" (List.length t)
end

module TR: RULES = struct
  (* candidates as binary trees, allowing to cut some branches during
     rewriting *)
  type t = L of set | N of (set*t*t)
  let empty = L Set.empty
  let rec xpass skipped t z = match t with
    | L x -> skipped, Set.union x z
    | N(x,tx,fx) -> 
      if Set.subseteq x z then 
	let skipped,z = xpass skipped tx z in 
	xpass skipped fx z
      else
	let skipped,z = xpass skipped fx z in 
	N(x,tx,skipped),z
  let pass = xpass empty
  let add x x' =
    let rec add' = function
    (* optimisable *)
    | L y -> L (Set.union y x') 
    | N(y,t,f) -> N(y,t,add' f)
    in
    let rec add = function
    | L y as t -> 
      if Set.subseteq x y then L (Set.union y x') 
      else N(x,L (Set.union y x'),t)
    | N(y,t,f) -> match Set.set_compare x y with
	| `Eq -> N(y,add' t,f)
	| `Lt -> N(x,N(y,add' t,L x'),f)
	| `Gt -> N(y,add t,f)
	| `N  -> N(y,t,add f)
    in add
  let rec size = function
    | L _ -> 1 | N(_,l,r) -> size l + size r
  let record ?stats t = Stats.record ?stats "final size of rules tree" (size t)
end

(* more functions on rules, derived from the one-pass rewriting process *)
module R: sig
  type t
  val empty: t
  val add: set -> set -> t -> t
  val norm: t -> set -> set
  val pnorm: t -> set -> set -> (t*set) option
  val norm': ('a -> set -> 'a*set) -> t -> 'a -> set -> set
  val pnorm': ('a -> set -> 'a*set) -> t -> 'a -> set -> set -> (t*set) option  
  val record: ?stats:Stats.collector -> t -> unit
end = struct
  (* selecting the implementation of rules *)
  include TR                    

  (* get the normal form of [x] *)
  let rec norm rules x =
    let rules,x' = pass rules x in
    if Set.equal x x' then x else norm rules x'

  (* get the normal form of [y], unless [x] is subsumed by this normal
     form. In the first case, also return the subset of rules that
     were not applied *)
  let pnorm rules x y = 
    let rec pnorm rules y =
      if Set.subseteq x y then None else
	let rules,y' = pass rules y in
	if Set.equal y y' then Some (rules,y') else pnorm rules y'
    in pnorm rules y

  (* get the normal form of [x] w.r.t a relation and a todo list *)
  let norm' f =
    let rec norm' rules todo x =
      let rules,x' = pass rules x in
      let todo,x'' = f todo x' in 
      if Set.equal x x'' then x else norm' rules todo x''
    in norm'

  (* get the normal form of [y] w.r.t a relation, unless [x] is
     subsumed by the normal form of [y] w.r.t a relation and a todo
     list. In the first case, the normal form is only w.r.t the
     relation, and we also return the subset of (relation) rules that
     were not applied. *)
  let pnorm' f rules todo x y =
    let rec pnorm' rules todo y =
      if Set.subseteq x y then true else
	let todo,y' = f todo y in 
	let rules,y'' = pass rules y' in
	if Set.equal y y'' then false else
	  pnorm' rules todo y''
    in
    match pnorm rules x y with
      | None -> None
      | Some(rules,y') as r -> if pnorm' rules todo y' then None else r
end


(* module DupRules(R1: ERULES)(R2: ERULES): ERULES = struct
 *   let massert b = if not b then failwith "Bug encountered (New.DupRules)"
 *   type t = R1.t * R2.t 
 *   let empty = R1.empty,R2.empty
 *   let add x y (t1,t2) = R1.add x y t1, R2.add x y t2
 *   let norm (t1,t2) x =
 *     let x1 = R1.norm t1 x in
 *     let x2 = R2.norm t2 x in
 *     massert(Set.equal x1 x2); x1
 *   let pnorm (t1,t2) x y =
 *     match R1.pnorm t1 x y, R2.pnorm t2 x y with
 *       | None, None -> None
 *       | Some(t1,y1), Some(t2,y2) -> massert(Set.equal y1 y2); Some((t1,t2),y1)
 *       | _ -> massert false; None
 *   let norm' f (t1,t2) todo x =
 *     let x1 = R1.norm' f t1 todo x in
 *     let x2 = R2.norm' f t2 todo x in
 *     massert(Set.equal x1 x2); x1
 *   let pnorm' f (t1,t2) todo x y =
 *     match R1.pnorm' f t1 todo x y, R2.pnorm' f t2 todo x y with
 *       | None, None -> None
 *       | Some(t1,y1), Some(t2,y2) -> massert(Set.equal y1 y2); Some((t1,t2),y1)
 *       | _ -> massert false; None
 * end *)


module TRW: UPTO = struct
  let name = "trw"
  type t = R.t ref
  let create ?stats _ = ref R.empty
  let unify_incl r x y =
    let r' = !r in
    (* TODO: lazier (normalisation of the union)? *)
    (* TODO: idem for TRW' below *)
    match R.pnorm r' x y with
    | None -> true
    | Some(ry,y') -> r := R.add y (R.norm ry (Set.union x y')) r'; false
  let unify_eq r x y =
    let r' = !r in
    match R.pnorm r' x y, R.pnorm r' y x with
    | None, None -> true
    | Some(ry,y'), None -> 
       r := R.add y (R.norm ry (Set.union x y')) r'; false
    | None, Some(rx,x') -> 
       r := R.add x (R.norm rx (Set.union x' y)) r'; false
    | Some(ry,y'), Some(_,x') -> 
       let z = R.norm ry (Set.union x' y') in
       r := R.add x z (R.add y z r'); false
  let unify ~incl = if incl then unify_incl else unify_eq
  let record ?stats m = R.record ?stats !m
end

module TRW'(Q: QUEUE): UPTO' = struct
  let name = "trw'"
  let strategy = Q.name
  type id = unit
  type t = R.t ref * (unit*letter list*set*set) Q.t ref
  let create ?stats _ = ref R.empty, ref Q.empty
  let push ~incl (_,q) w x y = q := Q.push !q ((),w,x,y)
  let pop (_,q) = match Q.pop !q with Some(r,q') -> q:=q'; Some r | None -> None
  let step incl todo z = Q.fold (fun (_,_,x,y as xy) (todo,z) ->
    if Set.subseteq y z then todo, Set.union x z else
      if not incl && Set.subseteq x z then todo, Set.union y z else
	Q.push todo xy,z
  ) todo (Q.empty,z)
  let skip ~incl (r,todo) _ x y =
    let r' = !r in
    let todo = !todo in
    let r_add1 x z =
      if debug `RW then Printf.printf " %a -> %a\n" Set.print x Set.print z;
      r := R.add x z r'
    in
    let r_add2 x y z =
      if debug `RW then Printf.printf " %a,%a -> %a\n" Set.print x Set.print y Set.print z;
      r := R.add x z (R.add y z r')
    in
    if incl then
      match R.pnorm' (step true) r' todo x y with
      | None -> true
      | Some(ry,y') -> r_add1 y (R.norm ry (Set.union x y')); false
    else
      match R.pnorm' (step false) r' todo x y, R.pnorm' (step false) r' todo y x with
      | None, None -> true
      (* TOTHINK: we need to add both pairs below, since success in one direction might 
         be due to the todo list ; could be better to add only one rule when success 
         is due only to R *)
      | Some(ry,y'), None -> r_add2 x y (R.norm ry (Set.union x y')); false
      | None, Some(rx,x') -> r_add2 x y (R.norm rx (Set.union x' y)); false
      | Some(ry,y'), Some(_,x') -> r_add2 x y (R.norm ry (Set.union x' y')); false
  let record ?stats (m,_) = R.record ?stats !m
end
