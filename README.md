Prototype implementation of the algorithm HKCw for checking inclusion or equivalence of non-deterministic Büchi automata (NBW), based on the implementation of algorithm HKC for automata on finite words (http://perso.ens-lyon.fr/damien.pous/hkc/)


requirements: OCaml, camlp4, ocamlbuild

compile with [make]
(or [make native] if you want the native version)

an NBW with two sets of initial states is provided in file example.ba
you can run HKCw as follows:

./hkcw.byte -equiv example.ba

run [./hkcw.byte -help] to get a full list of options

the option -sim is not described in the paper; it makes it possible to
exploit an up to similarity technique.

set [debug] to false in file common.ml and recompile if you don't need
to see execution traces.


Current restrictions:
- automata are parsed using Büchi accepting states rather than Büchi transitions
- up to unions/equivalence in the Büchi transition monoid is not implemented



Authors:
Denis Kuperberg, Laureline Pinault, Damien Pous.

firstname.lastname@ens-lyon.fr

Plume Team, LIP, ENS de Lyon, CNRS, UMR 5668
46 allée d'Italie, 69364 Lyon, FRANCE

