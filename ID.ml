open Common
type t = (set*set,unit) Hashtbl.t
let name = "id"
let create ?stats n = Hashtbl.create n
let unify ~incl m x y =
  if Hashtbl.mem m (x,y) then true
  else (Hashtbl.add m (x,y) (); false)
let record ?stats _ = ()
