(* union-find data structures *)

open Common

(* union-find as an up-to technique for bisimulations *)
module UF': UPTO     (* with setmap used recursively *)
module UF: UPTO      (* with a setmap used at toplevel (then refs) *)


(* union-find with tags *)
module type  TUF = sig
  type 'a t
   
  (* create a union find structure, where singleton classes are
   decorated  with the given function (lazily) *)
  val create: (set -> 'a) -> int -> 'a t
    
  (* unify the classes of two sets, using the given function to merge
   the decorations; returns true if the sets were already in the same
   class or  if the merging function returned true *)
  val unify: ('a -> 'a -> 'a * bool) -> 'a t -> set -> set -> bool

  (* get the tag of the equivalence class of a set *)
  val tag: 'a t -> set -> 'a
    
  (* iterate over all existing tags *)
  val iter: ('a -> unit) -> 'a t -> unit
end

module TUF': TUF     (* with setmap used recursively *)
module TUF: TUF      (* with a setmap used at toplevel (then refs) *)
