%{
  let app x y = match x,y with
    | [],z | z,[] -> z
    | _ -> List.rev_append x y
        
  let (^) (i1,i2,t,f) (i1',i2',t',f') =
    app i1 i1', app i2 i2', app t t', app f f'
  let product is ts fs =
        Util.fold (fun i ->
            Util.fold (fun t ->
                Util.fold (fun f acc -> (i,t,f)::acc) fs
              ) ts
          ) is []
%}
%token <string> ID
%token EOF INIT1 INIT2 FINAL LPAR RPAR NL DASH TO SIZE

%type <string list*string list*(string*string*string) list*string list> nfa
%start nfa

%%

nfa:
| EOF { [],[],[],[] }
| line NL nfa { $1 ^ $3 }

line:
| { [],[],[],[] }
| SIZE id NL { [],[],[],[] }
| INIT1 fset { $2,[],[],[] }
| INIT2 fset { [],$2,[],[] }
| FINAL fset { [],[],[],$2 }
| id id id { [],[],[$1,$2,$3],[] }
| fset DASH fset TO fset { [],[],product $1 $3 $5,[] }

fset:
| { [] }
| id fset { $1::$2 }

id:
| ID { $1 }
| LPAR id RPAR { $2 }
