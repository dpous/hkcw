type t = int ref
type collector = (string,int ref) Hashtbl.t * (string,float) Hashtbl.t


let create () = Hashtbl.create 10,Hashtbl.create 10
let global = create ()
           
let counter ?(stats=global) s =
  let t = fst stats in
  try Hashtbl.find t s
  with Not_found -> let r = ref 0 in Hashtbl.add t s r; r
  
let time_call ?(stats=global) ?gc s f x =
  let r,t = Util.time ?gc f x in
  Hashtbl.add (snd stats) s t;
  r

let count_calls ?stats s f = 
  let r = counter ?stats s in
  fun x -> incr r; f x

let incr ?(n=1) r = r := !r+n

let record ?stats s n = incr ~n (counter ?stats s)

let get ?(stats=global) s = try !(Hashtbl.find (fst stats) s) with Not_found -> 0

let get_time ?(stats=global) s = try Hashtbl.find (snd stats) s with Not_found -> nan

let print f (t,t') =
  let l = Hashtbl.fold (fun s i l -> (s,`I !i)::l) t [] in
  let l = Hashtbl.fold (fun s x l -> (s,`F x)::l) t' l in
  let l = List.sort (fun x y -> compare (fst x) (fst y)) l in
  List.iter (function
      | (s,`I i) -> Printf.fprintf f "// %s: %i\n" s i
      | (s,`F x) -> Printf.fprintf f "// %s: %.3fs\n" s x
    ) l

let reset (t,t') =
  Hashtbl.iter (fun _ r -> r:=0) t;
  Hashtbl.clear t'

let mean l = List.fold_left (+.) 0. l /. float_of_int (List.length l)

let min = List.fold_left (min) infinity 

let max = List.fold_left (max) neg_infinity 

let quantile p l =
  if l=[] then nan else
  let l = List.sort compare l in
  List.nth l (int_of_float (p *. float_of_int (List.length l)))

let median = quantile 0.5

