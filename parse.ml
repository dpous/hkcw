open Common
open NFA
open Parser

(* vata format *)

let print ?(compact=true) f (i,a) =
  let print_state = a.print_state in
  Printf.fprintf f "@NFA\n%%Initial";
  Set.iter (Printf.fprintf f " %a" print_state) i;
  Printf.fprintf f "\n%%Final";
  Set.iter (Printf.fprintf f " %a" print_state) a.accept;
  Printf.fprintf f "\n";
  Array.iteri (fun v ->
   Array.iteri (fun i x ->
    if compact && Set.size x>1 then (
      Printf.fprintf f "%a -%s->" print_state i (a.letter_name v);
      Set.iter (Printf.fprintf f " %a" print_state) x;
      Printf.fprintf f "\n"
    ) else
     Set.iter (fun j ->
      Printf.fprintf f "%a %s %a\n"
      print_state i (a.letter_name v) print_state j) x
    )
    ) a.delta

let from_lexbuf l =
  let i1,i2,t,o = Parser.nfa Lexer.token l in
  Misc.nfa_of_lists' i1 i2 t o

let from_lexbuf_ l =
  let i,_,t,o = Parser.nfa Lexer.token l in
  Misc.nfa_of_lists i t o

let from_lexbufs l l' =
  Misc.nfa_union (from_lexbuf_ l) (from_lexbuf_ l')

let open_close f h =
  let i = open_in f in
  try let x = h i in close_in i; x
  with e -> close_in i; raise e
  
let from_file f =
  open_close f (fun i -> 
  from_lexbuf (Lexing.from_channel i))

let from_files f g =
  open_close f (fun i ->  
  open_close g (fun j -> 
  from_lexbufs (Lexing.from_channel i) (Lexing.from_channel j)))
