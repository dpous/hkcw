(* abstract signature for finite sets of natural numbers *)

module type T = sig
  type t
  val name: string
  val empty: t
  val union: t -> t -> t
  val inter: t -> t -> t
  val singleton: int -> t
  val mem: int -> t -> bool
  val equal: t -> t -> bool
  val equal': t -> t -> int option
  val compare: t -> t -> int
  val full: int -> t
  val hash: t -> int
  val fold: (int -> 'a -> 'a) -> t -> 'a -> 'a
  val shift: int -> t -> t
  val size: t -> int
  val rem: int -> t -> t
  val elements: t -> int list
  val add: int -> t -> t
  val is_empty: t -> bool
  val intersect: t -> t -> bool
  val diff: t -> t -> t
  val subseteq: t -> t -> bool
  val set_compare: t -> t -> [`Lt|`Eq|`Gt|`N]
  val map: (int -> int) -> t -> t
  val iter: (int -> unit) -> t -> unit
  val filter: (int -> bool) -> t -> t

  val forall: t -> (int -> bool) -> bool
  val exists: t -> (int -> bool) -> bool

  val to_list: t -> int list    (* = elements *)
  val of_list: int list -> t

  val print: out_channel -> t -> unit
  val print': (out_channel->int->unit) -> out_channel -> t -> unit

  (* [random n p] returns a set whose elements are stricly 
     lesser than [n], and appear with a probability [p]  *)
  val random: int -> float -> t
  (* [random' n] returns a set whose elements are stricly 
     lesser than [n], where sets of a given cardinality 
     appear with equal chances *)
  val random': int -> t

  module Map: Hashtbl.S with type key = t
  val emptymap: string -> int -> 'a Map.t
end

(* sets as ordered lists *)
module OList: T

(* sets as integers (so that elements have to be really small) *)
module Small: T

(* sets as lists of integers (seen as bitmasks) *)
module IntList: T

(* sets as sparse lists of integers (seen as bitmasks) *)
module IntSparseList: T

(* sets as binary balanced trees (i.e., OCaml stdlib) *)
module AVL: T


module Dup(M1: T)(M2: T): T
