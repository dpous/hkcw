(* printing integers *)
let output_int f = Printf.fprintf f "%i"

(* printing lists *)
let rec print_list ?(sep=",") elt f = function
  | [] -> ()
  | [x] -> elt f x
  | x::q -> Printf.fprintf f "%a%s%a" elt x sep (print_list ~sep elt) q
                                     
(* timing a function call *)
let time ?(gc=true) f x =
  if gc then Gc.compact();
  let t0 = Sys.time() in
  let r = f x in
  r,Sys.time()-.t0
let print_time ?gc msg f x =
  let r,t = time ?gc f x in
  Printf.eprintf "%s: %.3fs\n%!" msg t;
  r

(* filtering and reversing a list *)
let filter_rev f =
  let rec xfilter_rev acc = function
    | [] -> acc
    | x::q when f x -> xfilter_rev (x::acc) q
    | _::q -> xfilter_rev acc q
  in xfilter_rev []

(* tail-rec [List.fold_left] with the type of [List.fold_right] *)
let rec fold f l a = match l with [] -> a | x::q -> fold f q (f x a)

(* formatted failwith *)
let failwith fmt = Printf.ksprintf failwith fmt
