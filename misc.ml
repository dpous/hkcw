open Util
open Common
open NFA

(* pretty-printing automata *)
          
let print_nfa_transitions f a =
  Array.iteri (fun v -> Array.iteri (fun i x -> 
    if not (Set.is_empty x) then
      Printf.fprintf f "%a -%s-> %a\n" a.print_state i (a.letter_name v) (Set.print' a.print_state) x;
  )) a.delta
let print_nfa f a =
  print_nfa_transitions f a;
  Printf.fprintf f "accept: %a\n" (Set.print' a.print_state) a.accept
let print_nfa' f (i,a) =
  Printf.fprintf f "init: %a\n" (Set.print' a.print_state) i;
  print_nfa f a
let print_nfa'' f (i,j,a) =
  Printf.fprintf f "init(0): %a\n" (Set.print' a.print_state) i;
  Printf.fprintf f "init(1): %a\n" (Set.print' a.print_state) j;
  print_nfa f a

let print_dfa f a =
  DFA.(
    Printf.fprintf f "start: %i\n" a.start;
    Array.iteri (fun v -> Array.iteri (fun i x -> 
      Printf.fprintf f "%a -%s-> %a\n" a.print_state i (a.letter_name v) a.print_state x;
      )) a.delta;
    Printf.fprintf f "accept: %a\n" (Set.print' a.print_state) a.accept)

(* forgetting state names *)
let anonymise_nfa a = NFA.{a with print_state=output_int}
let anonymise_dfa a = DFA.{a with print_state=output_int}

(* building NFA from lists *)
  
let xnfa_of_lists size letters delta_ accept_ print_state letter_name =
  let delta = Array.init letters (fun _ -> Array.make size Set.empty) in
  let accept = Set.of_list accept_ in
  List.iter (fun (i,v,j) -> 
      delta.(v).(i) <- Set.add j delta.(v).(i)
    ) delta_;
  { size; delta; accept; print_state; letter_name }

module SMap = Map.Make(struct type t=string let compare=compare end)
let nfa_of_lists' x y delta accept = 
  let get_idx m s =
    try SMap.find s !m
    with Not_found -> let i = SMap.cardinal !m in m:=SMap.add s i !m; i
  in
  let to_array m =
    match SMap.choose_opt m with
    | None -> [||]
    | Some (x,_) ->
       let t = Array.make (SMap.cardinal m) x in
       SMap.iter (fun x i -> t.(i) <- x) m;
       t
  in
  let letters = ref SMap.empty in
  let states = ref SMap.empty in
  let x = Set.of_list (List.rev_map (get_idx states) x) in
  let y = Set.of_list (List.rev_map (get_idx states) y) in
  let accept = List.rev_map (get_idx states) accept in
  let delta =
    List.rev_map
      (fun (i,v,j) -> (get_idx states i, get_idx letters v, get_idx states j))
      delta
  in
  let letters = to_array !letters in
  let states = to_array !states in
  let print_state f i = output_string f states.(i) in
  x,y,xnfa_of_lists (Array.length states) (Array.length letters)
    delta accept print_state (Array.get letters)
let nfa_of_lists x delta accept =
  let (x,_,a) = nfa_of_lists' x [] delta accept in (x,a)

let nfa_of_ilists delta accept =
  let max a b = assert (a >= 0 && b >= 0); max a b in
  let letters,size =
    let m = List.fold_left max 0 accept in
    List.fold_left (fun (w,k) (i,v,j) -> (max v w, max (max i j) k)) (0,m) delta
  in
  xnfa_of_lists (size+1) (letters+1) delta accept output_int string_of_int

(* seeing a DFA as a NFA *)
let nfa_of_dfa a = 
  let size = a.DFA.size in 
  Set.singleton a.DFA.start, {
    size = size;
    delta = Array.map 
      (fun dv -> Array.init size (fun i -> Set.singleton dv.(i))) 
      a.DFA.delta;
    accept = a.DFA.accept;
    print_state = a.DFA.print_state;
    letter_name = a.DFA.letter_name;
  }

(* reversing a NFA *)
let reverse_nfa = 
  let transpose n d =
    let d' = Array.make n Set.empty in
    for i=0 to n-1 do
      Set.fold (fun j () -> 
	d'.(j) <- Set.add i d'.(j)
      ) d.(i) ()
    done;
    d'
  in 
  fun (start,a) -> a.accept, { a with
    accept = start;
    delta = Array.map (transpose a.size) a.delta;
  }

(* reversing a DFA *)
let reverse_dfa =
  (* fun a -> reverse_nfa (nfa_of_dfa a) *)
  let transpose n d =
    let d' = Array.make n Set.empty in
    for i=0 to n-1 do
      let j = d.(i) in
      d'.(j) <- Set.add i d'.(j)
    done;
    d'
  in 
  fun a -> a.DFA.accept, { 
    size = a.DFA.size;
    accept = Set.singleton a.DFA.start;
    delta = Array.map (transpose a.DFA.size) a.DFA.delta;
    print_state = a.DFA.print_state;
    letter_name = a.DFA.letter_name;
  }

(* disjoint union of NFA *)
let nfa_union ?(dsl=false) (a_start,a) (b_start,b) =
  let m = ref SMap.empty in
  for i = 0 to letters a-1 do
    m := SMap.add (a.letter_name i) i !m;
  done;
  let r = ref (letters a-1) in
  for i = 0 to letters b-1 do
    (*OCAML4.06: m := SMap.update (b.letter_name i) (fun None -> incr r; Some !r | v -> v) !m *)
    let bi = b.letter_name i in
    if not (SMap.mem bi !m) then (incr r; m:=SMap.add bi !r !m)
  done;
  let letters = Array.make (!r+1) "" in
  SMap.iter (fun s i -> letters.(i) <- s) !m;
  let get_b v = SMap.find (b.letter_name v) !m in
  let a_size = a.size in
  let size = a_size+b.size in
  let shift = Set.shift a_size in
  let b_start = shift b_start in
  let delta = Array.init (!r+1) (fun _ -> Array.make size Set.empty) in
  Array.iteri (fun v -> Array.iteri (fun i dvi -> delta.(v).(i) <- dvi)) a.delta;
  Array.iteri (fun v -> Array.iteri (fun i dvi -> delta.(get_b v).(a_size+i) <- shift dvi)) b.delta;
  let accept = Set.union a.accept (shift b.accept) in
  let print_state0 f x =
    if x<a_size then Printf.fprintf f "(0,%a)" a.print_state x
    else Printf.fprintf f "(1,%a)" b.print_state x
  in
  let print_state1 f x =
    if x<a_size then a.print_state f x
    else b.print_state f (x-a_size)
  in
  let print_state = if dsl then print_state0 else print_state1 in
  a_start, b_start, { size; delta; accept; print_state; letter_name=Array.get letters }

let nfa_union ?dsl (x,a) (y,b) =
  if NFA.size a <= NFA.size b then nfa_union ?dsl (x,a) (y,b)
  else let (y,x,ab) = nfa_union ?dsl (y,b) (x,a) in (x,y,ab)

(* strings from sets *)
let string_of_set f x =
  match Set.to_list x with
  | [] -> "{}"
  | [x] -> "{"^f x^"}"
  | x::q -> "{"^f x^fold (fun y s -> ","^f y^s) q "}"

(* accessible subset construction for NFA determinisation
   forget names is set to true
 *)
let determinise ?(anon=false) (a_start,a) =
  let letters = letters a in
  let t = Set.Map.create 101 in
  let next = ref 0 in
  let accept = ref Set.empty in
  let rec get x: int =
    try fst(Set.Map.find t x)
    with Not_found -> 
          let x' = !next in
          next := x'+1;
          if Set.intersect x a.accept then
	    accept := Set.add x' !accept;
          let dx = Array.make letters x' in
          Set.Map.add t x (x',dx);
          for v = 0 to letters-1 do	    
	    dx.(v) <- get (delta_set a v x)
          done;
          x'
  in
  let start = get a_start in
  let delta = Array.init letters (fun _ -> Array.make !next start) in  
  let print_state =
    if anon then output_int else
      let back = Array.make !next Set.empty in
      Set.Map.iter (fun xi (i,di) -> back.(i) <- xi) t;
      fun f i -> Set.print' a.print_state f back.(i)
  in
  Set.Map.iter (fun xi (i,di) ->
      for v = 0 to letters-1 do
        delta.(v).(i) <- di.(v)
      done) t;
  DFA.{
      size = !next;
      start;
      delta;
      accept = !accept;
      print_state;
      letter_name = a.NFA.letter_name;
  }

(* generating a random NFA *)
let random_nfa ?(ne=false) ~n ~v ~r ~pa =
  let d = (if ne then r -. 1. else r) /. float_of_int n in
  let random_set() =
    if ne then Set.add (Random.int n) (Set.random n d)
    else Set.random n d
  in
  { size = n;
    delta = Array.init v (fun _ -> 
      Array.init n (fun _ -> random_set ()));
    accept = Set.random n pa;
    print_state = output_int;
    letter_name = string_of_int;
  }

(* generating two random NFA *)
let random_nfa2 ~n ~v ~r ~pa =
  let x0 = Set.add 0 (Set.rem 1 (Set.random n 0.5)) in
  let x1,n0 = Set.fold 
    (fun i (x,n0) -> if Set.mem i x0 then (x,n0+1) else Set.add i x,n0) 
    (Set.full n) (Set.empty,0) 
  in
  let d0 = r /. float_of_int n0 in
  let d1 = r /. float_of_int (n-n0) in
  { size = n;
    delta = 
      Array.init v (fun _ -> 
      Array.init n (fun i -> 
        if Set.mem i x0 then Set.inter x0 (Set.random n d0)
        else Set.inter x1 (Set.random n d1)
      ));
    accept = Set.random n pa;
    print_state = output_int;
    letter_name = string_of_int;
  }

let empty_nfa = {size=0; delta=[||]; accept=Set.empty; print_state=output_int; letter_name=(fun _ -> assert false)}

(* dispatching function *)
let random_nfa ?(disjoint=false) ?ne ~n ~v ~r ~pa =
  if disjoint then random_nfa2 ~n ~v ~r ~pa
  else random_nfa ?ne ~n ~v ~r ~pa

(* co-accessible states of a NFA *)
let co_accessible a = 
  let rev = Array.make a.size Set.empty in
  let rec loop x acc =
    Set.fold (fun i acc -> 
      if Set.mem i acc then acc
      else loop rev.(i) (Set.add i acc)
    ) x acc 
  in
  for v=0 to letters a-1 do
    for i=0 to a.size-1 do
      Set.fold (fun j () -> rev.(j) <- Set.add i rev.(j)) a.delta.(v).(i) ()
    done
  done;
  loop a.accept Set.empty 

(* live states of a NFA *)
let live (i,a) = 
  let next x = 
    Array.fold_right (fun d -> 
      Set.fold (fun i -> Set.union d.(i)) x) 
      a.delta Set.empty 
  in
  let coacc = co_accessible a in
  let rec loop x acc =
    let x = Set.inter x coacc in
    if Set.is_empty x then acc
    else 
      let acc = Set.union x acc in 
      loop (Set.diff (next x) acc) acc
  in
  loop i Set.empty

(* normalise a NFA by removing useless states (and reindexing states) *)
let normalise_states' (i,j,a) =
  let live = live (Set.union i j,a) in
  let t = Array.of_list (Set.to_list live) in
  let size = Array.length t in
  let t' = Array.make a.size None in
  Array.iteri (fun i j -> t'.(j) <- Some i) t;
  let map x = Set.fold (fun i acc ->
    match t'.(i) with
      | Some j -> Set.add j acc
      | None -> acc
  ) x Set.empty in
  let delta = 
    Array.init (letters a) (fun v ->
      Array.init size (fun i -> 
        map a.delta.(v).(t.(i))))
  in
  map i, map j, {
      size; delta; accept=map a.accept;
      print_state = (fun f x -> a.print_state f t.(x));
      letter_name = a.letter_name;
    }
let normalise_states (i,a) =
  let (i,_,a) = normalise_states' (i,Set.empty,a) in (i,a)

(* normalise a NFA by removing useless letters *)
let normalise_letters a =
  let non_empty t = 
    try for i=0 to Array.length t-1 do 
        if not (Set.is_empty t.(i)) then raise Not_found
      done; false
    with Not_found -> true
  in
  let letters,delta,names =
    Array.fold_right (fun t (i,d,n) ->
        if non_empty t then (i-1,t::d,a.letter_name i::n)
        else (i-1,d,n)) a.delta (letters a-1,[],[])
  in 
  let delta = Array.of_list delta in
  let names = Array.of_list names in 
  { a with delta; letter_name = Array.get names }

(* normalise a NFA, removing spurious states and letters *)
let normalise_nfa m =
  let (x,a) = normalise_states m in
  (x,normalise_letters a)
let normalise_nfa' m =
  let (x,y,a) = normalise_states' m in
  (x,y,normalise_letters a)

(* number of transisitions in a NFA *)
let transitions a =
  Array.fold_left (Array.fold_left (fun n x -> n+Set.size x)) 0 a.delta

(* maximal out-degree of a NFA *)
let out_degrees a =
  let ds =
    Array.of_list
      (Array.fold_right
         (Array.fold_right
            (fun x m -> Set.size x :: m))
         a.delta [])
  in
  let n = Array.length ds in
  Array.fast_sort compare ds;
  ds.(0),
  ds.(n/2),
  ds.((9*n)/10),
  ds.(n-1)
