BUILD=ocamlbuild

byte: 
	$(BUILD) hkcw.byte

native: 
	$(BUILD) hkcw.native

clean:
	ocamlbuild -clean
#	rm -rf _build 
