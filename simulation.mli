open Common

(* type of relations which should be contained in language inclusion *)
type t

(* largest simulation for a NFA *)
val largest: nfa -> t
val largest_naive: nfa -> t
val largest_focs95: nfa -> t
val largest_libvata: nfa -> t

(* is a state simulated by another one *)
val lower: t -> int -> int -> bool
val compare: t -> int -> int -> [`Eq | `Gt | `Lt | `N]

(* is a set simulated by another one *)
val set_lower: t -> set -> set -> bool

(* not so efficient *)
val saturate: t -> set -> set           (* downward closure *)
val minimise: t -> set -> set           (* select maximal elements *)
val set_compare: t -> set -> set -> [`Eq | `Gt | `Lt | `N]

(* saturate all transitions of an nfa *)
val saturate_nfa: t -> nfa -> nfa
val saturate_nfa': t -> set*nfa -> set*nfa
val saturate_nfa'': t -> set*set*nfa -> set*set*nfa

(* reduce an nfa to equate all states that are two similar *)
val reduce_nfa: t -> set*nfa -> t*(set*nfa)
val reduce_nfa': t -> set*set*nfa -> t*(set*set*nfa)

(* fully reduce an NFA (reduction + saturation) *)
val fully_reduce_nfa: set*nfa -> set*nfa
val fully_reduce_nfa': set*set*nfa -> set*set*nfa

(* print the simulation relation *)
val print: out_channel -> t -> unit

(* number of non-trivial pairs *)
val size: t -> int

(* number of non-trivial reflexive pairs *)
val kernel_size: t -> int

(* from a list of subsumptions *)
val of_list: int -> (int list * int) list -> t
